let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//~ mix.js('resources/assets/js/app.js', 'public/js')
   //~ .sass('resources/assets/sass/app.scss', 'public/css');
   
mix.styles([
    'public/css/bootstrap.css',
    'public/css/metisMenu.min.css',
    'public/css/morris.css',
    'public/css/font-awesome.min.css',
    'public/css/sb-admin-2.min.css',
    'public/datetime/css/bootstrap-datetimepicker.min.css',

    // 'vendor/datatables/datatables/media/css/jquery.dataTables.min.css',
    'vendor/datatables/datatables/media/css/dataTables.bootstrap.min.css',
    // 'vendor/datatables/datatables/media/css/dataTables.jqueryui.min.css

    'public/calendar/calendar.min.css',
    'public/css/chosen.min.css'

], 'public/css/app.css');

mix.scripts([
    'public/js/jquery.js',
    // 'public/js/jquery.min.js',
    'public/js/bootstrap.min.js',
    'public/js/metisMenu.min.js',
    'public/js/raphael.min.js',
    'public/js/morris.min.js',
    'public/js/moment-with-locales.js',
    'public/datetime/js/bootstrap-datetimepicker.min.js',

    'vendor/datatables/datatables/media/js/jquery.dataTables.min.js',
    'vendor/datatables/datatables/media/js/dataTables.bootstrap.min.js',

    //~ 'public/js/morris-data.js',
    'public/js/sb-admin-2.min.js',

    'vendor/components/underscore/underscore-min.js',

    'public/calendar/language/id-ID.js',

    'public/calendar/calendar.min.js',

    'public/js/chosen.jquery.js',

    'public/js/custom.js'
    
], 'public/js/app.js');
