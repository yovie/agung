@extends('welcome')

@section('content')

	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">Petugas Assessment</h1>
        </div>
	</div>

	<div class="row" style="min-height:600px;">
		<div class="col-md-12">

			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			
			<button class="btn btn-primary" data-toggle="modal" data-target="#form"> <span class="fa fa-plus"></span> Tambah Petugas</button>

			<br/>
			<br/>

			<table class="table table-bordered">
				<thead class="small">
					<tr>
						<th width="30px">No</th>
						<th>NIP</th>
						<th>Nama</th>
						<th>HP</th>
						<th>Email</th>
						<th width="50px"></th>
					</tr>
				</thead>
				<tbody>
					@foreach($petugas as $no=>$ass)
					<tr>
						<td>{{ $no+1 }}</td>
						<td>{{ $ass->nip }}</td>
						<td>{{ $ass->nama }}</td>
						<td>{{ $ass->hp }}</td>
						<td>{{ $ass->email }}</td>
						<td align="center">
							<button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#form" data-detail='{{ json_encode($ass) }}'> <span class="fa fa-edit"></span> </button>
							<button class="btn btn-danger btn-xs" onclick="doDelete('Yakin akan dihapus?','{{ url('assessment-hapus') }}',{{ '{id:' .$ass->id. ',_token:\'' .csrf_token(). '\'}' }})"> <span class="fa fa-remove"></span> </button>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

		</div>
	</div>

	<div id="form" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Tambah Assessment</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" method="post" action="{{ url('assessment') }}">
						{{ csrf_field() }}

						<input type="hidden" name="id">

						<div class="form-group">
							<label class="col-md-2">NIP</label>
							<div class="col-md-5">
								<input type="text" name="nip" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2">Nama</label>
							<div class="col-md-10">
								<input type="text" name="nama" class="form-control" required="required" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2">HP</label>
							<div class="col-md-5">
								<input type="text" name="hp" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-2">Email</label>
							<div class="col-md-7">
								<input type="email" name="email" class="form-control" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-7">
								<button class="btn btn-success">Simpan &nbsp; <span class="fa fa-save"></span></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>


	<script type="text/javascript">
		
		$('.table').dataTable();


		$('#form').on('shown.bs.modal', function (e) {
		    var b = $(e.relatedTarget);

		    var detail = b.data('detail');

		    $('#form').find('input[name=id]').val('');

		    if(detail)
		    {
		    	$('#form').find('input[name=id]').val(detail.id);
		    	$('#form').find('input[name=nama]').val(detail.nama);
		    	$('#form').find('input[name=hp]').val(detail.hp);
		    	$('#form').find('input[name=email]').val(detail.email);
		    	$('#form').find('input[name=nip]').val(detail.nip);
		    }
		} );

	</script>

@endsection