@extends('welcome')

@section('content')

	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">Profil</h1>
        </div>
	</div>

	<div class="row" style="min-height:600px;">
		<div class="col-md-12">

			@if ($errors->any())
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif

			@if($message)
				<div class="alert alert-{{ ($type=='error') ? 'danger':'success' }}">
			        <p>{{ $message }}</p>
			    </div>
			@endif

			<div class="col-md-6"> 
				<form method="post" class="form-horizontal">
					{{ csrf_field() }}

					<div class="form-group">
						<label class="col-md-2">Nama</label>
						<div class="col-md-8">
							<input type="text" name="name" placeholder="Nama" class="form-control" value="{{ $me->name }}" required="required" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-2">Email</label>
						<div class="col-md-8">
							<input type="email" name="email" placeholder="Email" class="form-control" value="{{ $me->email }}" required="required" />
						</div>
					</div>
					<div class="form-group">
						<button class="btn btn-success">Update &nbsp; <span class="fa fa-save"></span></button>
					</div>
				</form>
			</div>

			<div class="col-md-6"> 
				<form method="post" class="form-horizontal">	
					{{ csrf_field() }}

					<div class="form-group">
						<label class="col-md-4">Password Lama</label>
						<div class="col-md-6">
							<input type="password" name="pwdlama" placeholder="Password Lama" class="form-control" required="required" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Password Baru</label>
						<div class="col-md-6">
							<input type="password" name="pwdbaru" placeholder="Password Baru" class="form-control" required="required" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4">Password Baru</label>
						<div class="col-md-6">
							<input type="password" name="pwdlagi" placeholder="Password Baru Lagi" class="form-control" required="required" />
						</div>
					</div>
					<div class="form-group">
						<button class="btn btn-success">Update Password &nbsp; <span class="fa fa-save"></span></button>
					</div>
				</form>
			</div>


		</div>
	</div>

@endsection