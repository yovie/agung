@extends('welcome')

@section('content')

	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header"> {{ $view->nama_lengkap }} &nbsp; <a href="{{ url('registrasi') }}" class="btn btn-primary btn-xs"> <span class="fa fa-arrow-left"></span> &nbsp; Daftar Register</a></h1>
        </div>
	</div>

	<div class="row" style="min-height:600px;">

		<div class="col-lg-12">


			<div class="form-horizontal">

				<div class="form-group">
					<label class="col-md-9 text-right">Tanggal Assessment</label>
					<div class="col-md-3">
						{{ Gen::human_date($view->tanggal_assessment) }}
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-9 text-right">Jam Assessment</label>
					<div class="col-md-3">
						{{ Gen::human_time($view->jam_assessment) }}
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-9 text-right">Petugas Assessment</label>
					<div class="col-md-3">
						{{ ($view->petugasAssessment) ? $view->petugasAssessment->nama:'' }}
					</div>
				</div>

				<div id="step1">
					<h3>Keterangan Siswa</h3><br/>
					<div class="form-group">
						<label class="col-md-3">Nama Lengkap</label>
						<div class="col-md-5">
							{{ $view->nama_lengkap or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Nama Panggilan</label>
						<div class="col-md-4">
							{{ $view->nama_panggilan or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Jenis Kelamin</label>
						<div class="col-md-4">
							{{ ($view and $view->gender=='L') ? 'Laki-laki':'' }}
							{{ ($view and $view->gender=='P') ? 'Perempuan':'' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Tempat Lahir</label>
						<div class="col-md-4">
							{{ $view->tempat_lahir or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Tanggal Lahir</label>
						<div class="col-md-3">
							{{ Gen::human_date($view->tanggal_lahir) }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Agama</label>
						<div class="col-md-3">
							{{ $list_agama[$view->agama] }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Kewarganegaraan</label>
						<div class="col-md-4">
							{{ $list_kewarganegaraan[$view->kewarganegaraan] }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Anak ke</label>
						<div class="col-md-2">
							{{ $view->anak_ke or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Jumlah Saudara Kandung</label>
						<div class="col-md-2">
							{{ $view->jumlah_saudara_kandung or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Jumlah Saudara Tiri</label>
						<div class="col-md-2">
							{{ $view->jumlah_saudara_tiri or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Jumlah Saudara Angkat</label>
						<div class="col-md-2">
							{{ $view->jumlah_saudara_angkat or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Bahasa Sehari-hari</label>
						<div class="col-md-4">
							{{ $view->bahasa_sehari or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Berat Badan</label>
						<div class="col-md-2">
							{{ $view->berat_badan or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Tinggi Badan</label>
						<div class="col-md-2">
							{{ $view->tinggi_badan or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Golongan Darah</label>
						<div class="col-md-2">
							{{ $list_goldar[$view->golongan_darah] }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Penyakit Berat yang Pernah Diderita</label>
						<div class="col-md-6">
							{{ $view->penyakit_berat or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Berkebutuhan Khusus</label>
						<div class="col-md-6">
							{{ $view->berkebutuhan_khusus or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Alamat Tempat Tinggal</label>
						<div class="col-md-7">
							{{ $view->alamat or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Nomor Telepon</label>
						<div class="col-md-3">
							{{ $view->telepon or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">RT</label>
						<div class="col-md-2">
							{{ $view->rt or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">RW</label>
						<div class="col-md-2">
							{{ $view->rw or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Nama Dusun</label>
						<div class="col-md-5">
							{{ $view->nama_dusun or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Nama Kelurahan</label>
						<div class="col-md-5">
							{{ $view->nama_kelurahan or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Kecamatan</label>
						<div class="col-md-5">
							{{ $view->kecamatan or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Kode Pos</label>
						<div class="col-md-2">
							{{ $view->kode_pos or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Model Transportasi</label>
						<div class="col-md-5">
							{{ $view->model_transportasi or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Bertempat Tinggal pada</label>
						<div class="col-md-6">
							{{ $view->tinggal_pada or '' }}
						</div>
					</div>
				</div>

				<div id="step2" >
					<h3>Orang Tua/Wali</h3><br/>
					<h4>Nama</h4>
					<div class="form-group">
						<label class="col-md-3">Ayah Kandung</label>
						<div class="col-md-5">
							{{ $view->nama_ayah_kandung or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Ibu Kandung</label>
						<div class="col-md-5">
							{{ $view->nama_ibu_kandung or '' }}
						</div>
					</div>
					<h4>Pendidikan Tertinggi</h4>
					<div class="form-group">
						<label class="col-md-3">Ayah Kandung</label>
						<div class="col-md-3">
							{{$list_pendidikan[$view->pendidikan_ayah]}}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Ibu Kandung</label>
						<div class="col-md-3">
							{{$list_pendidikan[$view->pendidikan_ibu]}}
						</div>
					</div>
					<h4>Pekerjaan</h4>
					<div class="form-group">
						<label class="col-md-3">Ayah </label>
						<div class="col-md-5">
							{{ $view->pekerjaan_ayah or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Ibu </label>
						<div class="col-md-5">
							{{ $view->pekerjaan_ibu or '' }}
						</div>
					</div>
					<h4>Penghasilan</h4>
					<div class="form-group">
						<label class="col-md-3">Ayah </label>
						<div class="col-md-3">
							Rp. {{ $view->penghasilan_ayah or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Ibu </label>
						<div class="col-md-3">
							Rp. {{ $view->penghasilan_ibu or '' }}
						</div>
					</div>
					<h4>Tempat Lahir</h4>
					<div class="form-group">
						<label class="col-md-3">Ayah </label>
						<div class="col-md-5">
							{{ $view->tempatlahir_ayah or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Ibu </label>
						<div class="col-md-5">
							{{ $view->tempatlahir_ibu or '' }}
						</div>
					</div>
					<h4>Tanggal Lahir</h4>
					<div class="form-group">
						<label class="col-md-3">Ayah </label>
						<div class="col-md-3">
							{{ Gen::human_date($view->tanggallahir_ayah) }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Ibu </label>
						<div class="col-md-3">
							{{ Gen::human_date($view->tanggallahir_ibu) }}
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3">Nama Wali Siswa</label>
						<div class="col-md-5">
							{{ $view->nama_wali or '' }}
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3">Pendidikan Tertinggi</label>
						<div class="col-md-3">
							{{$list_pendidikan[$view->pendidikan_wali]}}
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3">Hubungan Terhadap Anak</label>
						<div class="col-md-5">
							{{ $view->hubungan_wali or '' }}
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3">Pekerjaan</label>
						<div class="col-md-5">
							{{ $view->pekerjaan_wali or '' }}
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3">Penghasilan</label>
						<div class="col-md-3">
							Rp. {{ $view->penghasilan_wali or '' }}
						</div>
					</div>

				</div>

				<div id="step3">
					<h3>Asal Mula Anak</h3><br/>
					<div class="form-group">
						<label class="col-md-3">Masuk Sekolah Ini Sebagai</label>
						<div class="col-md-7">
							{{ $view->masuk_sebagai or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Asal Anak</label>
						<div class="col-md-7">
							{{ $view->asal_anak or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Nama Taman Kanak-kanak</label>
						<div class="col-md-7">
							{{ $view->nama_tk or '' }}
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Nomor/Tahun Surat Keterangan</label>
						<div class="col-md-7">
							{{ $view->nomor_tahun_surat or '' }}
						</div>
					</div>
				</div>

			</div>

		</div>

	</div>


@endsection