@extends('welcome')

@section('content')

	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">Form Registrasi &nbsp; <a href="{{ url('registrasi') }}" class="btn btn-primary btn-xs"> <span class="fa fa-arrow-left"></span> &nbsp; Daftar Register</a></h1>
        </div>
	</div>

	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif

	<div class="row" style="min-height:600px;">
		<dir class="col-md-12">
			
			<form class="form-horizontal" method="post" action="{{ url('registrasi/simpan') }}" onsubmit="return checkv(this);" novalidate="novalidate">
				{{ csrf_field() }}

				@if($edit)
				<input type="hidden" name="idp" value="{{ $edit->id }}" />
				@endif

				<div class="form-group">
					<label class="col-md-9 text-right">Tanggal Assessment</label>
					<div class="col-md-3">
						<div class='input-group date' id='datetimepickera'>
							<input name="tanggal_assessment" type="text" class="form-control" required="true" value="{{ $edit->tanggal_assessment or '' }}" />
							<span class="input-group-addon">
								<span class="fa fa-calendar"></span>
							</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-9 text-right">Waktu Assessment</label>
					<div class="col-md-2">
						<div class='input-group date' id='datetimepickerj'>
							<input name="jam_assessment" type="text" class="form-control" required="true" value="{{ $edit->jam_assessment or '' }}"/>
							<span class="input-group-addon">
								<span class="fa fa-clock-o"></span>
							</span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-9 text-right">Petugas Assessment</label>
					<div class="col-md-3">
						<select name="petugas_assessment" required="required" class="form-control">
							<option></option>
							@foreach($list_petugas as $va=>$petugas)
								<option value="{{ $petugas->id }}" {{ ($edit and $edit->petugas_assessment==$petugas->id) ? 'selected':'' }} >{{ $petugas->nama }}</option>
							@endforeach
						</select>
					</div>
				</div>
				
				<div id="step1" class="small">
					<h3>Keterangan Siswa</h3><br/>
					<div class="form-group">
						<label class="col-md-3">Nama Lengkap</label>
						<div class="col-md-5">
							<input type="text" name="nama_lengkap" required="required" class="form-control" value="{{ $edit->nama_lengkap or '' }}" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Nama Panggilan</label>
						<div class="col-md-4">
							<input type="text" name="nama_panggilan" class="form-control" value="{{ $edit->nama_panggilan or '' }}" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Jenis Kelamin</label>
						<div class="col-md-4">
							<label> <input type="radio" name="gender" value="L" required="required" class="" {{ ($edit and $edit->gender=='L') ? 'checked="true"':'' }} /> &nbsp; Laki-laki</label> <br/>
							<label> <input type="radio" name="gender" value="P" required="required" class="" {{ ($edit and $edit->gender=='P') ? 'checked="true"':'' }} /> &nbsp; Perempuan</label>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Tempat Lahir</label>
						<div class="col-md-4">
							<input type="text" name="tempat_lahir" class="form-control" required="true" value="{{ $edit->tempat_lahir or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Tanggal Lahir</label>
						<div class="col-md-3">
							<div class='input-group date' id='datetimepicker'>
								<input name="tanggal_lahir" type="text" class="form-control" required="true" value="{{ $edit->tanggal_lahir or '' }}"/>
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Agama</label>
						<div class="col-md-3">
							<select name="agama" required="required" class="form-control">
								@foreach($list_agama as $va=>$agama)
									<option value="{{ $va }}" {{ ($edit and $edit->agama==$va) ? 'selected':'' }} >{{ $agama }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Kewarganegaraan</label>
						<div class="col-md-4">
							<select name="kewarganegaraan" required="required" class="form-control">
								@foreach($list_kewarganegaraan as $va=>$warga)
									<option value="{{ $va }}" {{ ($edit and $edit->kewarganegaraan==$va) ? 'selected':'' }} >{{ $warga }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Anak ke</label>
						<div class="col-md-2">
							<input type="number" name="anak_ke" class="form-control" value="{{ $edit->anak_ke or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Jumlah Saudara Kandung</label>
						<div class="col-md-2">
							<div class="input-group">
								<input type="number" name="jumlah_saudara_kandung" class="form-control" value="{{ $edit->jumlah_saudara_kandung or '' }}"/>
								<span class="input-group-addon">Orang</span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Jumlah Saudara Tiri</label>
						<div class="col-md-2">
							<div class="input-group">
								<input type="number" name="jumlah_saudara_tiri" class="form-control" value="{{ $edit->jumlah_saudara_tiri or '' }}"/>
								<span class="input-group-addon">Orang</span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Jumlah Saudara Angkat</label>
						<div class="col-md-2">
							<div class="input-group">
								<input type="number" name="jumlah_saudara_angkat" class="form-control" value="{{ $edit->jumlah_saudara_angkat or '' }}"/>
								<span class="input-group-addon">Orang</span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Bahasa Sehari-hari</label>
						<div class="col-md-4">
							<input type="text" name="bahasa_sehari" class="form-control" value="{{ $edit->bahasa_sehari or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Berat Badan</label>
						<div class="col-md-2">
							<div class="input-group">
								<input type="number" name="berat_badan" class="form-control" value="{{ $edit->berat_badan or '' }}"/>
								<span class="input-group-addon">Kg</span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Tinggi Badan</label>
						<div class="col-md-2">
							<div class="input-group">
								<input type="number" name="tinggi_badan" class="form-control" value="{{ $edit->tinggi_badan or '' }}"/>
								<span class="input-group-addon">Cm</span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Golongan Darah</label>
						<div class="col-md-2">
							<select name="golongan_darah" required="required" class="form-control">
								@foreach($list_goldar as $va=>$gol)
									<option value="{{ $va }}"  {{ ($edit and $edit->golongan_darah==$va) ? 'selected':'' }} >{{ $gol }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Penyakit Berat yang Pernah Diderita</label>
						<div class="col-md-6">
							<input type="text" name="penyakit_berat" class="form-control" value="{{ $edit->penyakit_berat or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Berkebutuhan Khusus</label>
						<div class="col-md-6">
							<input type="text" name="berkebutuhan_khusus" class="form-control" value="{{ $edit->berkebutuhan_khusus or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Alamat Tempat Tinggal</label>
						<div class="col-md-7">
							<textarea name="alamat" class="form-control" >{{ $edit->alamat or '' }}</textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Nomor Telepon</label>
						<div class="col-md-3">
							<input type="text" name="telepon" class="form-control" value="{{ $edit->telepon or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">RT</label>
						<div class="col-md-2">
							<input type="text" name="rt" class="form-control" value="{{ $edit->rt or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">RW</label>
						<div class="col-md-2">
							<input type="text" name="rw" class="form-control" value="{{ $edit->rw or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Nama Dusun</label>
						<div class="col-md-5">
							<input type="text" name="nama_dusun" class="form-control" value="{{ $edit->nama_dusun or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Nama Kelurahan</label>
						<div class="col-md-5">
							<input type="text" name="nama_kelurahan" class="form-control" value="{{ $edit->nama_kelurahan or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Kecamatan</label>
						<div class="col-md-5">
							<input type="text" name="kecamatan" class="form-control" value="{{ $edit->kecamatan or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Kode Pos</label>
						<div class="col-md-2">
							<input type="text" name="kode_pos" class="form-control" value="{{ $edit->kode_pos or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Model Transportasi</label>
						<div class="col-md-5">
							<input type="text" name="model_transportasi" class="form-control" value="{{ $edit->model_transportasi or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Bertempat Tinggal pada</label>
						<div class="col-md-6">
							<input type="text" name="tinggal_pada" class="form-control" value="{{ $edit->tinggal_pada or '' }}"/>
						</div>
					</div>
				</div>

				<div id="step2" >
					<h3>Orang Tua/Wali</h3><br/>
					<h4>Nama</h4>
					<div class="form-group">
						<label class="col-md-3">Ayah Kandung</label>
						<div class="col-md-5">
							<input type="text" name="nama_ayah_kandung" class="form-control" value="{{ $edit->nama_ayah_kandung or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Ibu Kandung</label>
						<div class="col-md-5">
							<input type="text" name="nama_ibu_kandung" class="form-control" value="{{ $edit->nama_ibu_kandung or '' }}"/>
						</div>
					</div>
					<h4>Pendidikan Tertinggi</h4>
					<div class="form-group">
						<label class="col-md-3">Ayah Kandung</label>
						<div class="col-md-3">
							<select name="pendidikan_ayah" class="form-control">
								@foreach($list_pendidikan as $va=>$pen)
									<option value="{{ $va }}"   {{ ($edit and $edit->pendidikan_ayah==$va) ? 'selected':'' }} >{{ $pen }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Ibu Kandung</label>
						<div class="col-md-3">
							<select name="pendidikan_ibu" class="form-control">
								@foreach($list_pendidikan as $va=>$pen)
									<option value="{{ $va }}"  {{ ($edit and $edit->pendidikan_ibu==$va) ? 'selected':'' }} >{{ $pen }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<h4>Pekerjaan</h4>
					<div class="form-group">
						<label class="col-md-3">Ayah </label>
						<div class="col-md-5">
							<input type="text" name="pekerjaan_ayah" class="form-control"  value="{{ $edit->pekerjaan_ayah or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Ibu </label>
						<div class="col-md-5">
							<input type="text" name="pekerjaan_ibu" class="form-control"  value="{{ $edit->pekerjaan_ibu or '' }}"/>
						</div>
					</div>
					<h4>Penghasilan</h4>
					<div class="form-group">
						<label class="col-md-3">Ayah </label>
						<div class="col-md-3">
							<div class="input-group">
								<span class="input-group-addon">Rp.</span>
								<input type="number" name="penghasilan_ayah" class="form-control"  value="{{ $edit->penghasilan_ayah or '' }}"/>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Ibu </label>
						<div class="col-md-3">
							<div class="input-group">
								<span class="input-group-addon">Rp.</span>
								<input type="number" name="penghasilan_ibu" class="form-control"  value="{{ $edit->penghasilan_ibu or '' }}"/>
							</div>
						</div>
					</div>
					<h4>Tempat Lahir</h4>
					<div class="form-group">
						<label class="col-md-3">Ayah </label>
						<div class="col-md-5">
							<input type="text" name="tempatlahir_ayah" class="form-control"  value="{{ $edit->tempatlahir_ayah or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Ibu </label>
						<div class="col-md-5">
							<input type="text" name="tempatlahir_ibu" class="form-control"  value="{{ $edit->tempatlahir_ibu or '' }}"/>
						</div>
					</div>
					<h4>Tanggal Lahir</h4>
					<div class="form-group">
						<label class="col-md-3">Ayah </label>
						<div class="col-md-3">
							<div class='input-group date' id='datetimepicker1'>
								<input type="text" name="tanggallahir_ayah" class="form-control"  value="{{ $edit->tanggallahir_ayah or '' }}"/>
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Ibu </label>
						<div class="col-md-3">
							<div class='input-group date' id='datetimepicker2'>
								<input type="text" name="tanggallahir_ibu" class="form-control"  value="{{ $edit->tanggallahir_ibu or '' }}"/>
								<span class="input-group-addon">
									<span class="fa fa-calendar"></span>
								</span>
							</div>
						</div>
					</div>

					<hr/>

					<div class="form-group">
						<label class="col-md-3">Nama Wali Siswa</label>
						<div class="col-md-5">
							<input type="text" name="nama_wali" class="form-control"  value="{{ $edit->nama_wali or '' }}"/>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3">Pendidikan Tertinggi</label>
						<div class="col-md-3">
							<select name="pendidikan_wali" class="form-control">
								@foreach($list_pendidikan as $va=>$pen)
									<option value="{{ $va }}"  {{ ($edit and $edit->pendidikan_wali==$va) ? 'selected':'' }} >{{ $pen }}</option>
								@endforeach
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3">Hubungan Terhadap Anak</label>
						<div class="col-md-5">
							<input type="text" name="hubungan_wali" class="form-control"  value="{{ $edit->hubungan_wali or '' }}"/>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3">Pekerjaan</label>
						<div class="col-md-5">
							<input type="text" name="pekerjaan_wali" class="form-control"  value="{{ $edit->pekerjaan_wali or '' }}"/>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-3">Penghasilan</label>
						<div class="col-md-3">
							<div class="input-group">
								<span class="input-group-addon">Rp.</span>
								<input type="text" name="penghasilan_wali" class="form-control"  value="{{ $edit->penghasilan_wali or '' }}"/>
							</div>
						</div>
					</div>

				</div>

				<div id="step3">
					<h3>Asal Mula Anak</h3><br/>
					<div class="form-group">
						<label class="col-md-3">Masuk Sekolah Ini Sebagai</label>
						<div class="col-md-7">
							<input type="text" name="masuk_sebagai" class="form-control"  value="{{ $edit->masuk_sebagai or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Asal Anak</label>
						<div class="col-md-7">
							<input type="text" name="asal_anak" class="form-control"  value="{{ $edit->asal_anak or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Nama Taman Kanak-kanak</label>
						<div class="col-md-7">
							<input type="text" name="nama_tk" class="form-control"  value="{{ $edit->nama_tk or '' }}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3">Nomor/Tahun Surat Keterangan</label>
						<div class="col-md-7">
							<input type="text" name="nomor_tahun_surat" class="form-control"  value="{{ $edit->nomor_tahun_surat or '' }}"/>
						</div>
					</div>
				</div>

				<div class="form-group" id="btnNav">
					<div class="col-md-12 text-center">
						<button type="button" class="btn btn-lg btn-warning left"><span class="fa fa-arrow-left"></span></button> &nbsp; 
						<button type="button" class="btn btn-lg btn-warning right"><span class="fa fa-arrow-right"></span></button>
					</div>
				</div>

				<div class="form-group" id="btnSave">
					<div class="col-md-7">
						<button class="btn btn-lg btn-success">Simpan &nbsp; <span class="fa fa-save"></span></button>
					</div>
				</div>
			</form>

		</div>
	</dir>

	<script type="text/javascript">

		function checkv(t){
			var rr = true;
			rr = $('input[name=tanggal_assessment]')[0].validity.valid;
			if(!rr){
				alert('Tanggal Assessment harus diisi');
				return false;
			}
			rr = $('input[name=jam_assessment]')[0].validity.valid;
			if(!rr){
				alert('Jam Assessment harus diisi');
				return false;
			}
			rr = $('select[name=petugas_assessment]')[0].validity.valid;
			if(!rr){
				alert('Petugas Assessment harus diisi');
				return false;
			}
			rr = $('input[name=nama_lengkap]')[0].validity.valid;
			if(!rr){
				alert('Nama lengkap harus diisi');
				return false;
			}
			rr = $('input[name=jam_panggilan]')[0].validity.valid;
			if(!rr){
				alert('Nama panggilan harus diisi');
				return false;
			}
			return rr;
		}

		$(document).ready(function(){
			$('#datetimepicker').datetimepicker({
				locale: 'id',
				format: 'YYYY-MM-DD',
				maxDate: moment()
			});

			$('#datetimepicker1').datetimepicker({
				locale: 'id',
				format: 'YYYY-MM-DD',
				maxDate: moment()
			});

			$('#datetimepicker2').datetimepicker({
				locale: 'id',
				format: 'YYYY-MM-DD',
				maxDate: moment()
			});

			@if(empty($edit))
			$('#datetimepickera').datetimepicker({
				locale: 'id',
				format: 'YYYY-MM-DD',
				minDate: new Date()
			});
			@else
			$('#datetimepickera').datetimepicker({
				locale: 'id',
				format: 'YYYY-MM-DD'
			});
			@endif

			$('#datetimepickerj').datetimepicker({
				locale: 'id',
				format: 'HH:mm'
			});

			var step = 1;
			$('#step2, #step3').hide();
			@if(!$edit)
			$('#btnSave').hide();
			@endif
			$('#btnNav').find('.left').attr('disabled','true');

			$('#btnNav').find('.right').click(function(){
				if(step==1){
					$('#step1').hide();
					$('#step2').show();
					$('#btnNav').find('.left').removeAttr('disabled');
					step = 2;
				}else if(step==2){
					$('#step2').hide();
					$('#step3').show();
					$('#btnNav').find('.right').attr('disabled','true');
					$('#btnSave').show();
					step = 3;
				}
			});
			$('#btnNav').find('.left').click(function(){
				if(step==2){
					$('#step2').hide();
					$('#step1').show();
					$('#btnNav').find('.left').attr('disabled','true');
					step = 1;
				}else if(step==3){
					$('#step3').hide();
					$('#step2').show();
					$('#btnNav').find('.right').removeAttr('disabled');
					step = 2;
				}
			});
		});
	</script>

@endsection