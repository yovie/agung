@extends('welcome')

@section('content')

	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">Daftar Register &nbsp; <a href="{{ url('registrasi/baru') }}" class="btn btn-primary btn-xs"> <span class="fa fa-plus"></span> &nbsp; Form Registrasi</a></h1>
        </div>
	</div>

	<div class="row" style="min-height:600px;">

		<div class="col-lg-12">

		<table class="table table-bordered">
			<thead class="small">
				<tr>
					<th width="30px">No</th>
					<th>Nama Lengkap</th>
					<th>Nama Panggilan</th>
					<th>Alamat</th>
					<th>Tanggal Lahir</th>
					<th>Tanggal Daftar</th>
					<th>Tanggal Assessment</th>
					<th>Status</th>
					<th width="100px"></th>
				</tr>
			</thead>
			<tbody>
				@foreach($pasiens as $nom=>$pasien)
				<tr>
					<td>{{ $nom+1 }}</td>
					<td>{{ $pasien->nama_lengkap }}</td>
					<td>{{ $pasien->nama_panggilan }}</td>
					<td>{{ $pasien->alamat }}</td>
					<td>{{ Gen::human_date($pasien->tanggal_lahir) }}</td>
					<td>{{ Gen::human_date($pasien->created_at) }}</td>
					<td>{{ Gen::human_date($pasien->tanggal_assessment) }}</td>
					<td>
						@if($pasien->status=='register')
						<span style="color:black">Register</span>
						@endif
						@if($pasien->status=='assessment')
						<span style="color:green">Assessment</span>
						@endif
						@if($pasien->status=='terapi')
						<span style="color:blue">Terapi</span>
						@endif
					</td>
					<td align="center">
						<a href="{{ url('registrasi/output/' . $pasien->id) }}" class="btn btn-xs btn-default" title="Output"> <span class="fa fa-file"></span> </a>
						<a href="{{ url('registrasi/' . $pasien->id) }}" class="btn btn-xs btn-primary" title="Detail"> <span class="fa fa-list"></span> </a>
						<a href="{{ url('registrasi/baru/' . $pasien->id) }}" class="btn btn-xs btn-warning" title="Edit"> <span class="fa fa-edit"></span> </a>
						<a href="javascript:doDelete('Yakin akan dihapus?','{{ url('registrasi/hapus/') }}',{{ '{idp:' .$pasien->id. ',_token:\'' .csrf_token(). '\'}' }})" class="btn btn-xs btn-danger" title="Hapus"> <span class="fa fa-remove"></span> </a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>

		</div>
	</div>


	<script type="text/javascript">
		
		$('table').dataTable();

	</script>

@endsection