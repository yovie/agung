<table width="100%">
	<tbody>
		<tr>
			<td>
				<img src="img/logometro.png" height="75px"/>			
			</td>
			<td align="center" style="font-size: 10pt;font-weight: bold;padding: 0 10px 0 10px;">
			PEMERINTAH KOTA METRO<br/>
			DINAS PENDIDIKAN, KEBUDAYAAN, PEMUDA DAN OLAHRAGA<br/>
			UPT. PUSAT LAYANAN AUTIS (PLA) KOTA METRO<br/>
			Jln. Raya Stadion Kelurahan Tejosari Kecamatan.Metro Timur Kota Metro			
			</td>
			<td>
				<img src="img/logoupt.png" height="75px"/>			
			</td>
		</tr>
	</tbody>
</table>

<hr/>

<p style="text-align: right;">NP : {{ str_pad($pasien->np, 30, '.', STR_PAD_RIGHT) }}</p>

<p style="text-align: center;font-weight: bold;">
	FORMULIR PENDAFTARAN SISWA BARU<br/>
	<?php 
		$dt = Gen::to_date_time($pasien->created_at);
		$tahun = $dt->format('Y');
		$bulan = $dt->format('m');
		if(intval($bulan)<=6){
			$ta = intval($tahun)-1;
			$tb = intval($tahun);
		}else{
			$ta = intval($tahun);
			$tb = intval($tahun)+1;
		}
	?>
	TAHUN {{ $ta.'/'.$tb }}<br/>
</p>

<p style="font-weight: bold;"> A.   KETERANGAN SISWA</p>

	<table width="100%" style="margin-left: 20px;">
		<tbody>
			<tr><td width="280px">1. Nama Lengkap</td><td>:  &nbsp; {{ $pasien->nama_lengkap }}</td></tr>
			<tr><td>2. Nama Panggilan</td><td>:  &nbsp; {{ $pasien->nama_panggilan }}</td></tr>
			<tr><td>3. Jenis Kelamin</td><td>:  &nbsp; {{ $pasien->gender=='L' ? 'Laki-laki':'Perempuan' }}</td></tr>
			<tr><td>4. Tempat dan Tanggal Lahir</td><td>:  &nbsp; {{ $pasien->tempat_lahir .', '. Gen::human_date($pasien->tanggal_lahir) }}</td></tr>
			<tr><td>5. Agama</td><td>:  &nbsp; {{ $pasien->islam }}</td></tr>
			<tr><td>6. Kewarganegaraan</td><td>:  &nbsp; {{ strtoupper($pasien->kewarganegaraan) }}</td></tr>
			<tr><td>7. Anak Ke</td><td>:  &nbsp; {{ $pasien->anak_ke }}</td></tr>
			<tr><td>8. Jumlah Saudara Kandung</td><td>:  &nbsp; {{ $pasien->jumlah_saudara_kandung }}</td></tr>
			<tr><td>9. Jumlah Saudara Tiri</td><td>:  &nbsp; {{ $pasien->jumlah_saudara_tiri }}</td></tr>
			<tr><td>10. Jumlah Saudara Angkat</td><td>:  &nbsp; {{ $pasien->jumlah_saudara_angkat }}</td></tr>
			<tr><td>11. Bahasa Sehari-hari</td><td>:  &nbsp; {{ $pasien->bahasa_sehari }}</td></tr>
			<tr><td>12. Berat Badan</td><td>:  &nbsp; {{ $pasien->berat_badan }}</td></tr>
			<tr><td>13. Tinggi Badan</td><td>:  &nbsp; {{ $pasien->tinggi_badan }}</td></tr>
			<tr><td>14. Golongan Darah</td><td>:  &nbsp; {{ $pasien->golongan_darah }}</td></tr>
			<tr><td>15. Penyakit Berat yang Pernah Diderita</td><td>:  &nbsp; {{ $pasien->penyakit_berat }}</td></tr>
			<tr><td>16. Berkebutuhan Khusus</td><td>:  &nbsp; {{ $pasien->berkebutuhan_khusus }}</td></tr>
			<tr><td>17. Alamat Tempat Tinggal</td><td>:  &nbsp; {{ $pasien->alamat }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  Nomor Telepon</td><td>:  &nbsp; {{ $pasien->telepon }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  RT</td><td>:  &nbsp; {{ $pasien->rt }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  RW</td><td>:  &nbsp; {{ $pasien->rw }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  Nama Dusun</td><td>:  &nbsp; {{ $pasien->nama_dusun }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  Desa/Kelurahan</td><td>:  &nbsp; {{ $pasien->nama_kelurahan }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  Kecamatan</td><td>:  &nbsp; {{ $pasien->kecamatan }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  Kode Pos</td><td>:  &nbsp; {{ $pasien->kode_pos }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  Model Transportasi</td><td>:  &nbsp; {{ $pasien->model_transportasi }}</td></tr>
			<tr><td>18.Bertempat Tinggal Pada</td><td>:  &nbsp; {{ $pasien->tinggal_pada }}</td></tr>
		</tbody>
	</table>
	
<p style="font-weight: bold;margin-left: 20px;"> ORANG TUA / WALI</p>

	<table width="100%" style="margin-left: 20px;">
		<tbody>
			<tr><td width="280px">19. Nama</td><td></td></tr>
			<tr><td>  &nbsp;  &nbsp;  Ayah Kandung</td><td>:  &nbsp; {{ $pasien->nama_ayah_kandung }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  Ibu Kandung</td><td>:  &nbsp; {{ $pasien->nama_ibu_kandung }}</td></tr>
			<tr><td>20. Pendidikan Tertinggi</td><td></td></tr>
			<tr><td>  &nbsp;  &nbsp;  Ayah Kandung</td><td>:  &nbsp; {{ $pasien->pendidikan_ayah }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  Ibu Kandung</td><td>:  &nbsp; {{ $pasien->pendidikan_ibu }}</td></tr>
			<tr><td>21. Pekerjaan</td><td></td></tr>
			<tr><td>  &nbsp;  &nbsp;  Ayah</td><td>:  &nbsp; {{ $pasien->pekerjaan_ayah }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  Ibu</td><td>:  &nbsp; {{ $pasien->pekerjaan_ibu }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  Penghasilan</td><td></td></tr>
			<tr><td>  &nbsp;  &nbsp;  Ayah</td><td>:  &nbsp; {{ $pasien->penghasilan_ayah }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  Ibu</td><td>:  &nbsp; {{ $pasien->penghasilan_ibu }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  Tempat, Tanggal Lahir</td><td></td></tr>
			<tr><td>  &nbsp;  &nbsp;  Ayah</td><td>:  &nbsp; {{ $pasien->tempatlahir_ayah }}, {{ $pasien->tanggallahir_ayah }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  Ibu</td><td>:  &nbsp; {{ $pasien->tempatlahir_ibu }}, {{ $pasien->tanggallahir_ibu }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  Nama Wali Siswa</td><td>:  &nbsp; {{ $pasien->nama_wali }}</td></tr>
			<tr><td>22. Pendidikan Tertinggi</td><td>:  &nbsp; {{ $pasien->pendidikan_wali }}</td></tr>
			<tr><td>23. Hubungan Terhadap Anak</td><td>:  &nbsp; {{ $pasien->hubungan_wali }}</td></tr>
			<tr><td>24. Pekerjaan</td><td>:  &nbsp; {{ $pasien->pekerjaan_wali }}</td></tr>
			<tr><td>25. Penghasilan</td><td>:  &nbsp; {{ $pasien->penghasilan_wali }}</td></tr>
		</tbody>
	</table>


<p style="font-weight: bold;">B.   ASAL MULA ANAK</p>

	<table width="100%" style="margin-left: 20px;">
		<tbody>
			<tr><td width="280px">26. Masuk Sekolah ini Sebagai</td><td>:  &nbsp; {{ $pasien->masuk_sebagai }}</td></tr>
			<tr><td>27. A. Asal Anak</td><td>:  &nbsp; {{ $pasien->asal_anak }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  &nbsp;  B. Nama Taman Kanak-Kanak</td><td>:  &nbsp; {{ $pasien->nama_tk }}</td></tr>
			<tr><td>  &nbsp;  &nbsp;  &nbsp;  C. Nomor/Tahun Surat Keterangan</td><td>:  &nbsp; {{ $pasien->nomor_tahun_surat }}</td></tr>
		</tbody>
	</table>


<br/>
<p style="text-align: right;">Metro, {{ Gen::human_date($pasien->created_at) }}</p>
<br/>

<table width="100%">
	<tbody>
		<tr>
			<td style="text-align: center" valign="top">
			Mengetahui<br/>
			Kepala UPT.Pusat Layanaan Autis<br/><br/><br/><br/><br/>
			EKO ISMANTO, ST<br/>
			NIP.19750529005011007
			</td>
			<td style="text-align: center" valign="top">
			Orang Tua / WaliMurid<br/><br/><br/><br/><br/><br/>
			{{ str_pad( empty($pasien->nama_ayah) ? $pasien->nama_wali:$pasien->nama_ayah  , 30, '.', STR_PAD_BOTH) }}
			</td>
		</tr>
	</tbody>
</table>
