@extends('welcome')

@section('content')

	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">Selamat datang</h1>
        </div>
	</div>

	<div class="row" style="min-height:600px;">
		<div class="col-md-12">
			
			<h3>Login</h3>
			<ul>
				<li>Register, register@gmail.com/register</li>
				<li>Assessment, assessment@gmail.com/assessment</li>
				<li>Therapist, therapist@gmail.com/therapist</li>
				<li>Administrator, admin@gmail.com/admin</li>
			</ul>

			<h3>Progress</h3>
			<ul>
				<li>Proses Registrasi <span class="fa fa-check"></span> </li>
				<li>Proses Assessment</li>
				<li>Proses Terapi</li>
				<li>Jadwal <span class="fa fa-check"></span> </li>
				<li>Laporan-laporan</li>
				<li>Konfigurasi  <span class="fa fa-check"></span></li>
			</ul>

			<h3><a href="file:///home/yovie/Sources/agung/readme.md">README</a></h3>

		</div>
	</div>

@endsection