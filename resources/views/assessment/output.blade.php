<table width="100%">
	<tbody>
		<tr>
			<td>
				<img src="img/logometro.png" height="70px"/>			
			</td>
			<td align="center" style="font-size: 10pt;font-weight: bold;padding: 0 10px 0 10px;">
			PEMERINTAH KOTA METRO<br/>
			DINAS PENDIDIKAN, KEBUDAYAAN, PEMUDA DAN OLAHRAGA<br/>
			UPT. PUSAT LAYANAN AUTIS (PLA) KOTA METRO<br/>
			Jln. Raya Stadion Kelurahan Tejosari Kecamatan.Metro Timur Kota Metro			
			</td>
			<td>
				<img src="img/logoassessment.png" height="70px"/>			
			</td>
		</tr>
	</tbody>
</table>

<hr/>

<p style="text-align: center;font-weight: bold;">Wawancara Perkembangan Anak</p>

	<table width="75%" border="0" style="margin-left: 22%">
		<thead>
			<tr><td width="160px">Nama</td><td>: {{ $ass->pasien->nama_lengkap }}</td></tr>
			<tr><td>Tempat, Tanggal lahir</td><td>: {{ $ass->pasien->tempat_lahir }}, {{ Gen::human_date($ass->pasien->tanggal_lahir) }}</td></tr>
			<tr><td>Usia</td><td>: {{ Gen::calc_age($ass->pasien->tanggal_lahir) }} Tahun</td></tr>
			<tr><td>Tanggal assessment</td><td>: {{ Gen::human_date($ass->pasien->tanggal_assessment) }}</td></tr>
		</thead>
	</table>


<p style="font-weight: bold;">1. Riwayat Perkembangan Anak</p>

			<ol style="list-style: lower-alpha;">
				@for($c='A';$c<'E';$c++)
					@if( isset($aq[$c]) )
						<li>
							<?= $aq[$c]->question; ?>
							@if( isset($aq[$c.'1']) )
								<ul>
									@for($d=1;$d<50;$d++)
										@if( isset($aq[$c.$d]) )
											<li>
												<?= $aq[$c.$d]->question; ?>
												<div style="border:solid 1 black;padding: 5px;margin-bottom: 10px;">
													@if( isset($an[$c.$d]) )
													{{ $an[$c.$d]->answer }}
													@endif
												</div>
											</li>
										@endif
									@endfor
								</ul>
							@else
								<div style="border:solid 1 black;padding: 5px;margin-bottom: 10px;">
									@if( isset($an[$c]) )
									{{ $an[$c]->answer }}
									@endif
								</div>
							@endif
						</li>
					@endif
				@endfor
			</ol>

<p style="font-weight: bold;">2. Data Sosial</p>


			<ol style="list-style: lower-alpha;">
				@for($c='F';$c<'Z';$c++)
					@if( isset($aq[$c]) )
						<li>
							<?= $aq[$c]->question; ?>
							@if( isset($aq[$c.'1']) )
								<ul>
									@for($d=1;$d<50;$d++)
										@if( isset($aq[$c.$d]) )
											<li>
												<?= $aq[$c.$d]->question; ?>
												<div style="border:solid 1 black;padding: 5px;margin-bottom: 10px;">
													@if( isset($an[$c.$d]) )
													{{ $an[$c.$d]->answer }}
													@endif
												</div>	
											</li>
										@endif
									@endfor
								</ul>
							@else
								<div style="border:solid 1 black;padding: 5px;margin-bottom: 10px;">
									@if( isset($an[$c]) )
									{{ $an[$c]->answer }}
									@endif
								</div>
							@endif
						</li>
					@endif
				@endfor
			</ol>

<p style="font-weight: bold;">3. Rencana Jadwal Terapi</p>

<table width="100%" border="1" cellspacing="0">
	<thead>
		<tr>
			<th>Hari</th>
			<th>Waktu Pelaksanaan</th>
			<th>Keterangan</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><?php 
				$dd = Gen::to_date_time($ass->tanggal_terapi);
				if($dd)
					echo $dd->format('l');
			?></td>
			<td>{{ Gen::human_date($ass->tanggal_terapi) }} &nbsp; </td>
			<td>{{ $ass->description }} </td>
		</tr>
	</tbody>
</table>