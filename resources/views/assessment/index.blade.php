@extends('welcome')

@section('content')

	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">Assessment</h1>
        </div>
	</div>

	<div class="row" style="min-height:600px;">

		<div class="col-lg-12">

			<h3>Daftar Antrian Assessment</h3>

			<table class="table table-bordered">
				<thead class="small">
					<tr>
						<th width="30px">No</th>
						<th>Nama Lengkap</th>
						<th>Nama Panggilan</th>
						<th>Petugas Assessment</th>
						<th>Tanggal Lahir</th>
						<th>Tanggal Daftar</th>
						<th>Tanggal Assessment</th>
						<th>Status</th>
						<th width="30px"></th>
					</tr>
				</thead>
				<tbody>
					@foreach($pasiens as $nom=>$pasien)
					<tr>
						<td>{{ $nom+1 }}</td>
						<td>{{ $pasien->nama_lengkap }}</td>
						<td>{{ $pasien->nama_panggilan }}</td>
						<td>{{ $pasien->petugasAssessment->nama }}</td>
						<td>{{ Gen::human_date($pasien->tanggal_lahir) }}</td>
						<td>{{ Gen::human_date($pasien->created_at) }}</td>
						<td>{{ Gen::human_date($pasien->tanggal_assessment) }}</td>
						@if($pasien->tanggal_assessment<date('Y-m-d H:i:s'))
						<td style="color:red">Expired</td>
						@else
						<td style="color:green">{{ Gen::diff_days(time(), $pasien->tanggal_assessment) }} hari lagi</td>
						@endif
						<td align="center">
							<a href="{{ url('assessment/check/' . $pasien->id) }}" class="btn btn-xs btn-primary" title="Proses Assessment"> <span class="fa fa-arrow-right"></span> </a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

		</div>

		<div class="col-lg-12">
			
			<h3>Daftar Proses Assessment</h3>

			<table class="table table-bordered">
				<thead class="small">
					<tr>
						<th width="30px">No</th>
						<th>Nama Lengkap</th>
						<th>Nama Panggilan</th>
						<th>Petugas Assessment</th>
						<th>Tanggal Lahir</th>
						<th>Tanggal Daftar</th>
						<th>Tanggal Assessment</th>
						<th>Tanggal Terapi</th>
						<th>Status</th>
						<th width="70px"></th>
					</tr>
				</thead>
				<body>
					@foreach($assessments as $nom=>$ass)
					<tr>
						<td>{{ $nom+1 }}</td>
						<td>{{ $ass->pasien->nama_lengkap }}</td>
						<td>{{ $ass->pasien->nama_panggilan }}</td>
						<td>{{ $ass->petugasAssessment->nama }}</td>
						<td>{{ Gen::human_date($ass->pasien->tanggal_lahir) }}</td>
						<td>{{ Gen::human_date($ass->pasien->created_at) }}</td>
						<td>{{ Gen::human_date($ass->tanggal) }}</td>
						<td>{{ Gen::human_date($ass->tanggal_terapi) }}</td>
						<td><?php
							$total = $ass->answer->count();
							$jwb = $ass->answer->where('answer','<>',null)->count();
							$sisa = $total-$jwb;
							if($sisa>0)
								echo "Ada $sisa poin assessment yang belum diisi";
							else
								echo "Assessment sudah diisi";
						 ?></td>
						<td align="center">
							
							<a href="{{ url('assessment/output/' . $ass->id) }}" class="btn btn-xs btn-default" title="Output"> <span class="fa fa-file"></span> </a>

							@if($ass->pasien->status=='register')
							<button onclick="apply_assessment(this)" data-ida="<?= $ass->id ?>" class="btn btn-xs btn-success" title="Proses Assessment Selesai" {{ ($sisa>0) ? 'disabled':'' }} > <span class="fa fa-check"></span> </button>
							@endif

							<a href="{{ url('assessment/form/' . $ass->id) }}" class="btn btn-xs btn-primary" title="Proses Assessment"> <span class="fa fa-arrow-right"></span> </a>
						</td>
					</tr>
					@endforeach
				</body>
			</table>

		</div>

	</div>


	<script type="text/javascript">
		
		$('table').dataTable();

		function apply_assessment(th){
			var ida = $(th).data('ida');
			if(ida){
				if(confirm('Apakah akan dilanjut ke proses terapi ?')){
					$.post('{{ url('api/close-assessment') }}',{ida:ida,_token:'{{ csrf_token() }}'},function(res){
						// console.log(res);
						location.reload();
					});
				}
			}
		}

	</script>

@endsection