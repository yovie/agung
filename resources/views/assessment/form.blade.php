@extends('welcome')

@section('content')

	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">Assessment</h1>
        </div>
	</div>

	<div class="row" style="min-height:600px;">

		<div class="col-lg-12" style="margin-bottom: 60px">
			<label class="col-md-1 col-md-offset-3">Pasien</label>
			<div class="col-md-5">
				<select class="form-control" name="pasien">
					<option value="0"></option>
					@foreach($pasiens as $pa)
					<option value="{{ $pa->id }}" <?php if(!empty($anw)) echo ($pa->id==$anw->pasien_id) ? 'selected':''; ?> >{{ $pa->nama_lengkap }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="col-md-12 text-center form-horizontal">
			<h4>Wawancara Perkembangan Anak</h4>

			{{ csrf_field() }}
			
			<div class="form-group mt">
				<label class="col-md-3 col-md-offset-2 text-left">Nama</label>
				<div class="col-md-5">
					<input type="text" name="nama" class="form-control" placeholder="Nama" value="<?= empty($anw) ? '':$anw->pasien->nama_lengkap ?>" disabled />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-md-offset-2 text-left mt">Tempat, Tanggal lahir</label>
				<div class="col-md-2">
					<input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" value="<?= empty($anw) ? '':$anw->pasien->tempat_lahir ?>" disabled/>
				</div>
				<div class="col-md-3 ">
					<input type="text" name="tanggal_lahir" class="form-control" placeholder="Tanggal Lahir" value="<?= empty($anw) ? '':Gen::human_date($anw->pasien->tanggal_lahir) ?>" disabled/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-md-offset-2 text-left">Usia</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text" name="usia" class="form-control" placeholder="Usia"  value="<?= empty($anw) ? '':Gen::calc_age($anw->pasien->tanggal_lahir) ?>" disabled />
						<span class="input-group-addon">Tahun</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-md-offset-2 text-left">Tanggal Assessment</label>
				<div class="col-md-3">
					<input type="text" name="tanggal_assessment" class="form-control" placeholder="Tanggal Assessment" value="<?= empty($anw) ? '':Gen::human_date($anw->tanggal) ?>" disabled/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-md-offset-2 text-left">Petugas Assessment</label>
				<div class="col-md-4">
					<input type="text" name="petugas_assessment" class="form-control" placeholder="Petugas Assessment" value="<?= empty($anw) ? '':$anw->petugasAssessment->nama ?>" disabled/>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					@if(!empty($anw))
						@if(Gen::compare_date($anw->tanggal,date('Y-m-d'))==1)
							<label style="color:blue">Proses assessment masih {{ Gen::diff_days(time(), $anw->tanggal) }} hari lagi </label>
						@endif	
						@if(Gen::compare_date($anw->tanggal,date('Y-m-d'))==-1)
							<label style="color:red">Proses assessment sudah terlewat {{ Gen::diff_days(time(), $anw->tanggal) }} hari yang lalu </label>
						@endif	
						@if(Gen::compare_date($anw->tanggal,date('Y-m-d'))==0)
							<label style="color:green">Proses assessment hari ini </label>
						@endif	
					@endif	
				</div>
			</div>
		</div>

		<div class="col-md-12 mt">
			<ol>
				@for($c='A';$c<'Z';$c++)
					@if( isset($aq[$c]) )
						<li>
							<?= $aq[$c]->question; ?>
							@if( isset($aq[$c.'1']) )
								<ul>
									@for($d=1;$d<50;$d++)
										@if( isset($aq[$c.$d]) )
											<li>
												<?= $aq[$c.$d]->question; ?>
												<div class="col-md-12">
													<textarea class="form-control mb input" name="{{ $c.$d }}"></textarea>
												</div>
											</li>
										@endif
									@endfor
								</ul>
							@else
								<div class="col-md-12">
									<textarea class="form-control mb input" name="{{ $c }}"></textarea>
								</div>
							@endif
						</li>
					@endif
				@endfor
			</ol>
		</div>

		<div class="col-md-12 mt form-horizontal">
			<div class="form-group">
				<label class="col-md-2 col-md-offset-3 text-left">Tanggal Terapi</label>
				<div class="col-md-3">
					<div class='input-group date' id='datetimepicker2'>
						<input name="tanggal_terapi" type="text" class="form-control input" required="true" placeholder="Tanggal Terapi" value="" />
						<span class="input-group-addon">
							<span class="fa fa-calendar"></span>
						</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-md-offset-3 text-left">Petugas Terapi</label>
				<div class="col-md-3">
					<select name="petugas_terapi_id" class="form-control input" placeholder="Petugas Terapi">
						<option></option>
						@foreach($petugas as $pe)
						<option value="{{ $pe->id }}">{{ $pe->nip.' - '.$pe->nama }}</option>
						@endforeach
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-2 col-md-offset-3 text-left">Keterangan</label>
				<div class="col-md-4">
					<textarea name="description" class="form-control input" placeholder="Keterangan"></textarea>
				</div>
			</div>
		</div>

	</div>

	<script type="text/javascript">

		@if(empty($anw))
		window.anw = null;
		@else
		window.anw = <?= json_encode($anw) ?>;
		@endif
		
		$('select[name=pasien]').chosen();

		$('#datetimepicker2').datetimepicker({
				locale: 'id',
				format: 'YYYY-MM-DD'
				// minDate: new Date()
		}).on('dp.change', function(e){ 
			
			if(window.anw==null)
				return;

			var aid = window.anw.id;
			var field = 'tanggal_terapi';
			var value = e.date.format(e.date._f).substr(0,10);

			// console.log(aid,field,value);

			$.post('{{ url('api/save-assessment-info') }}',{aid:aid,field:field,value:value,_token:'{{ csrf_token() }}'},function(res){
				// console.log(res);
			});

		});

		var activateAction = function(){

			$('select[name=pasien]').change(function(){ 
				location.href="{{ url('assessment/check/') }}/" + this.value; 
			})

			$('input.input[name=tanggal_terapi], select.input[name=petugas_terapi_id], textarea[name=description]').change(function(){
				if(window.anw==null)
					return;

				var aid = window.anw.id;
				var field = $(this).attr('name');
				var value = $(this).val();

				$.post('{{ url('api/save-assessment-info') }}',{aid:aid,field:field,value:value,_token:'{{ csrf_token() }}'},function(res){
					// console.log(res);
				});
			});

			$('textarea.input').change(function(){
				if(window.anw==null)
					return;

				var aq = window.anw.id;
				var qcode = $(this).attr('name');
				var answer = $(this).val();

				if(qcode=='description')
					return;

				$.post('{{ url('api/save-assessment') }}',{aq:aq,qcode:qcode,answer:answer,_token:'{{ csrf_token() }}'},function(res){
					// console.log(res);
				});
			});

		}


		$(document).ready(function(){

			$('textarea.input').each(function(a,b){
				if(window.anw==undefined)
					return;

				var aq = window.anw.id;
				var qcode = $(this).attr('name');

				if(qcode=='description')
					return;

				$.get('{{ url('api/get-assessment') }}',{aq:aq,qcode:qcode},function(res){
					if(res.result){
						$(b).val(res.answer);
					}
				});
			});

			$('input.input[name=tanggal_terapi], select.input[name=petugas_terapi_id], textarea[name=description]').each(function(a,b){
				if(window.anw==null)
					return;

				var aid = window.anw.id;
				var field = $(this).attr('name');

				$.get('{{ url('api/get-assessment-info') }}',{aid:aid,field:field},function(res){
					if(res.result){
						$(b).val(res.value);
					}
				});
			})

			// console.log(cc);

			@if(!empty($anw))
				@if(Gen::compare_date($anw->tanggal,date('Y-m-d'))==-1 || $anw->pasien->status!='register')
			$('textarea.input').attr('disabled','disabled');
			$('select.input').attr('disabled','disabled');
			$('input.input').attr('disabled','disabled');
				@endif	
			@else
			$('textarea.input').attr('disabled','disabled');
			$('select.input').attr('disabled','disabled');
			$('input.input').attr('disabled','disabled');
			@endif	

			setTimeout(function(){
				activateAction();
			},1000);

		});

	</script>


@endsection