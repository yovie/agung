<table width="100%">
	<tbody>
		<tr>
			<td>
				<img src="img/logometro.png" height="70px"/>			
			</td>
			<td align="center" style="font-size: 10pt;font-weight: bold;padding: 0 10px 0 10px;">
			PEMERINTAH KOTA METRO<br/>
			DINAS PENDIDIKAN, KEBUDAYAAN, PEMUDA DAN OLAHRAGA<br/>
			UPT. PUSAT LAYANAN AUTIS (PLA) KOTA METRO<br/>
			Jln. Raya Stadion Kelurahan Tejosari Kecamatan.Metro Timur Kota Metro			
			</td>
			<td>
				<img src="img/logoupt.png" height="70px"/>			
			</td>
		</tr>
	</tbody>
</table>

<hr/>

<p style="text-align: center;font-weight: bold;">EVALUASI 8 BULANAN</p>

	<table border="0" style="font-weight: bold;">
		<thead>
			<tr><td width="130px">Nama</td><td>: {{ $select->nama_lengkap }}</td></tr>
			<tr><td>Jenis Terapi</td><td>: {{ $evaluasi[0]->jenis_terapi }}</td></tr>
		</thead>
	</table>


<div style="border: solid 1px black; padding: 0 5px 0 5px;margin-top:10px;">
	<?= $evaluasi_select->karakteristik ?>
</div>

<div style="border: solid 1px black; padding: 0 5px 0 5px;margin-top:10px;">
	<?= $evaluasi_select->program ?>
</div>

<div style="border: solid 1px black; padding: 0 5px 0 5px;margin-top:10px;">
	<?= $evaluasi_select->tujuan ?>
</div>

<div style="border: solid 1px black; padding: 0 5px 0 5px;margin-top:10px;">
	<?= $evaluasi_select->aktivitas ?>
</div>

<div style="border: solid 1px black; padding: 0 5px 0 5px;margin-top:10px;">
	<?= $evaluasi_select->evaluasi ?>
</div>


<br/>
<p style="text-align: right;">Metro, {{ Gen::human_date($evaluasi_select->created_at) }}</p>
<br/>

<table width="100%">
	<tbody>
		<tr>
			<td style="text-align: center" valign="top" width="50%">
			Orang Tua / WaliMurid<br/><br/><br/><br/><br/><br/>
			{{ str_pad( empty($select->nama_ayah) ? $select->nama_wali:$select->nama_ayah  , 30, '.', STR_PAD_BOTH) }}
			</td>
			<td style="text-align: center" valign="top">
			Terapis<br/><br/><br/><br/><br/><br/>
			{{ $select->getAssessment()->terapi()->petugasTerapi->nama }}
			</td>
		</tr>
	</tbody>
</table>
