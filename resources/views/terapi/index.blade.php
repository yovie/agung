@extends('welcome')

@section('content')

	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">Terapi</h1>
        </div>
	</div>

	<div class="row" style="min-height:600px;">

		<div class="col-lg-12">

			<h3>Daftar Antrian Terapi</h3>

			<table class="table table-bordered">
				<thead class="small">
					<tr>
						<th width="30px">No</th>
						<th>Nama Lengkap</th>
						<th>Nama Panggilan</th>
						<th>Petugas Terapi</th>
						<th>Tanggal Daftar</th>
						<th>Tanggal Assessment</th>
						<th>Tanggal Terapi</th>
						<th>Status</th>
						<th width="30px"></th>
					</tr>
				</thead>
				<tbody>
					@foreach($pasiens as $nom=>$pasien)
					<tr>
						<td>{{ $nom+1 }}</td>
						<td>{{ $pasien->nama_lengkap }}</td>
						<td>{{ $pasien->nama_panggilan }}</td>
						<td>{{ $pasien->getAssessment()->terapi()->petugasTerapi->nama }}</td>
						<td>{{ Gen::human_date($pasien->created_at) }}</td>
						<td>{{ Gen::human_date($pasien->tanggal_assessment) }}</td>
						<td>{{ Gen::human_date($pasien->getAssessment()->tanggal_terapi) }}</td>
						@if($pasien->getAssessment()->tanggal_terapi<date('Y-m-d H:i:s'))
						<td style="color:red">Expired</td>
						@else
						<td style="color:green">{{ Gen::diff_days(time(), $pasien->getAssessment()->tanggal_terapi) }} hari lagi</td>
						@endif
						<td align="center">
							<a href="{{ url('terapi/check/' . $pasien->getAssessment()->id) }}" class="btn btn-xs btn-primary" title="Home Program"> <span class="fa fa-home"></span> </a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

		</div>

		<div class="col-lg-12">
			
			<h3>Daftar Proses Terapi</h3>

			<table class="table table-bordered">
				<thead class="small">
					<tr>
						<th width="30px">No</th>
						<th>Nama Lengkap</th>
						<th>Nama Panggilan</th>
						<th>Alamat</th>
						<th>Tanggal Daftar</th>
						<th>Tanggal Assessment</th>
						<th>Tanggal Terapi</th>
						<th>Status</th>
						<th width="50px"></th>
					</tr>
				</thead>
				<body>
					@foreach($terapis as $nom=>$ass)
					<tr>
						<td>{{ $nom+1 }}</td>
						<td>{{ $ass->pasien->nama_lengkap }}</td>
						<td>{{ $ass->pasien->nama_panggilan }}</td>
						<td>{{ $ass->pasien->alamat }}</td>
						<td>{{ Gen::human_date($ass->pasien->created_at) }}</td>
						<td>{{ Gen::human_date($ass->pasien->tanggal_assessment) }}</td>
						<td>{{ Gen::human_date($ass->tanggal) }}</td>
						<td></td>
						<td align="center">
							
							<a href="{{ url('terapi/output/' . $ass->id) }}" class="btn btn-xs btn-default" title="Output"> <span class="fa fa-file"></span> </a>

							@if($ass->pasien->status=='register')
							<button onclick="apply_assessment(this)" data-ida="<?= $ass->id ?>" class="btn btn-xs btn-success" title="Proses Assessment Selesai" {{ ($sisa>0) ? 'disabled':'' }} > <span class="fa fa-check"></span> </button> &nbsp; 
							@endif

							<a href="{{ url('terapi/form/' . $ass->id) }}" class="btn btn-xs btn-primary" title="Home Program"> <span class="fa fa-home"></span> </a>
						</td>
					</tr>
					@endforeach
				</body>
			</table>

		</div>

	</div>


	<script type="text/javascript">
		
		$('table').dataTable();

		function apply_assessment(th){
			var ida = $(th).data('ida');
			if(ida){
				if(confirm('Apakah akan dilanjut ke proses terapi ?')){
					$.post('{{ url('api/close-assessment') }}',{ida:ida,_token:'{{ csrf_token() }}'},function(res){
						// console.log(res);
						location.reload();
					});
				}
			}
		}

	</script>

@endsection