<table width="100%">
	<tbody>
		<tr>
			<td>
				<img src="img/logometro.png" height="70px"/>			
			</td>
			<td align="center" style="font-size: 10pt;font-weight: bold;padding: 0 10px 0 10px;">
			PEMERINTAH KOTA METRO<br/>
			DINAS PENDIDIKAN, KEBUDAYAAN, PEMUDA DAN OLAHRAGA<br/>
			UPT. PUSAT LAYANAN AUTIS (PLA) KOTA METRO<br/>
			Jln. Raya Stadion Kelurahan Tejosari Kecamatan.Metro Timur Kota Metro			
			</td>
			<td>
				<img src="img/logoupt.png" height="70px"/>			
			</td>
		</tr>
	</tbody>
</table>

<hr/>

<p style="text-align: center;font-weight: bold;">HOME PROGRAM</p>

	<table border="0" style="font-weight: bold;">
		<thead>
			<tr><td width="130px">Nama</td><td>: {{ $ter->pasien->nama_lengkap }}</td></tr>
			<tr><td>Jenis Terapi</td><td>: {{ $ter->jenis }}</td></tr>
		</thead>
	</table>

<table border="1" cellspacing="0" width="100%" cellpadding="5px">
	<thead>
		<tr><th>Program</th><th>Aktivitas</th><th>Keterangan</th></tr>
	</thead>
	<tbody>
		@foreach($det as $m)
		<tr><td>{{ $m->program }}</td>
		<td>{{ $m->aktivitas }}</td>
		<td>{{ $m->keterangan }}</td></tr>
		@endforeach
	</tbody>
</table>

<p style="font-weight: bold;">Keterangan :</p>
<p>{{ $ter->description }}</p>


<br/>
<p style="text-align: right;">Metro, {{ Gen::human_date($ter->tanggal) }}</p>
<br/>

<table width="100%">
	<tbody>
		<tr>
			<td style="text-align: center" valign="top" width="50%">
			Orang Tua / WaliMurid<br/><br/><br/><br/><br/><br/>
			{{ str_pad( empty($ter->pasien->nama_ayah) ? $ter->pasien->nama_wali:$ter->pasien->nama_ayah  , 30, '.', STR_PAD_BOTH) }}
			</td>
			<td style="text-align: center" valign="top">
			Terapis<br/><br/><br/><br/><br/><br/>
			{{ $ter->petugasTerapi->nama }}
			</td>
		</tr>
	</tbody>
</table>
