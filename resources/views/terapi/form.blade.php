@extends('welcome')

@section('content')

	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">Terapi</h1>
        </div>
	</div>

	<div class="row" style="min-height:600px;">

		<div class="col-lg-12" style="margin-bottom: 60px">
			<label class="col-md-1 col-md-offset-3">Pasien</label>
			<div class="col-md-5">
				<select class="form-control" name="pasien">
					<option value="0"></option>
					@foreach($pasiens as $pa)
					<option value="{{ $pa->getAssessment()->id }}" <?php if(!empty($terapi)) echo ($pa->id==$terapi->pasien_id) ? 'selected':''; ?> >{{ $pa->nama_lengkap }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="col-md-12 text-center form-horizontal">
			<h4>Home Program</h4>

			{{ csrf_field() }}
			
			<div class="form-group mt">
				<label class="col-md-3 col-md-offset-2 text-left">Nama</label>
				<div class="col-md-5">
					<input type="text" name="nama" class="form-control" placeholder="Nama" value="<?= empty($terapi) ? '':$terapi->pasien->nama_lengkap ?>" disabled />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-md-offset-2 text-left mt">Tempat, Tanggal lahir</label>
				<div class="col-md-2">
					<input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" value="<?= empty($terapi) ? '':$terapi->pasien->tempat_lahir ?>" disabled/>
				</div>
				<div class="col-md-3 ">
					<input type="text" name="tanggal_lahir" class="form-control" placeholder="Tanggal Lahir" value="<?= empty($terapi) ? '':Gen::human_date($terapi->pasien->tanggal_lahir) ?>" disabled/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-md-offset-2 text-left">Usia</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text" name="usia" class="form-control" placeholder="Usia"  value="<?= empty($terapi) ? '':Gen::calc_age($terapi->pasien->tanggal_lahir) ?>" disabled />
						<span class="input-group-addon">Tahun</span>
					</div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-md-offset-2 text-left">Tanggal Assessment</label>
				<div class="col-md-3">
					<input type="text" name="tanggal_assessment" class="form-control" placeholder="Tanggal Assessment" value="<?= empty($terapi) ? '':Gen::human_date($terapi->pasien->tanggal_assessment) ?>" disabled/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-md-offset-2 text-left">Petugas Assessment</label>
				<div class="col-md-5">
					<input type="text" name="petugas_assessment" class="form-control" placeholder="Petugas Assessment" value="<?= empty($terapi) ? '':$terapi->pasien->petugasAssessment->nama ?>" disabled/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-md-offset-2 text-left">Tanggal Terapi</label>
				<div class="col-md-3">
					<input type="text" name="tanggal_terapi" class="form-control" placeholder="Tanggal Terapi" value="<?= empty($terapi) ? '':Gen::human_date($terapi->tanggal) ?>" disabled/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-md-offset-2 text-left">Petugas Terapi</label>
				<div class="col-md-5">
					<input type="text" name="petugas_terapi" class="form-control" placeholder="Petugas Terapi" value="<?= empty($terapi) ? '':$terapi->petugasTerapi->nama ?>" disabled/>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-12">
					@if(!empty($terapi))
						@if(Gen::compare_date($terapi->tanggal,date('Y-m-d'))==1)
							<label style="color:blue">Proses terapi masih {{ Gen::diff_days(time(), $terapi->tanggal) }} hari lagi </label>
						@endif	
						@if(Gen::compare_date($terapi->tanggal,date('Y-m-d'))==-1)
							<label style="color:red">Proses terapi sudah terlewat {{ Gen::diff_days(time(), $terapi->tanggal) }} hari yang lalu </label>
						@endif	
						@if(Gen::compare_date($terapi->tanggal,date('Y-m-d'))==0)
							<label style="color:green">Proses terapi hari ini </label>
						@endif	
					@endif	
				</div>
			</div>

			<hr/>

			<div class="form-group mt">
				<label class="col-md-3 col-md-offset-2 text-left">Jenis Terapi</label>
				<div class="col-md-4">
					<input type="text" name="jenis" class="form-control input" placeholder="Jenis Terapi" value="<?= empty($terapi) ? '':$terapi->jenis ?>" />
				</div>
			</div>
			<div class="form-group mt">
				<label class="col-md-3 col-md-offset-2 text-left">Keterangan</label>
				<div class="col-md-5">
					<textarea name="description" class="form-control input" rows="10" placeholder="Keterangan"><?= empty($terapi) ? '':$terapi->description ?></textarea>
				</div>
			</div>
		</div>

		<div class="col-md-12 mt">
			
			<table class="table table-bordered mt table-input">
				<thead>
					<tr>
						<th>Program</th>
						<th>Aktivitas</th>
						<th>Keterangan</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>

		</div>

	</div>

	<script type="text/javascript">

		@if(empty($terapi))
		window.terapi = null;
		@else
		window.terapi = <?= json_encode($terapi) ?>;
		@endif
		
		$('select[name=pasien]').chosen();


		var activateAction = function(){

			$('select[name=pasien]').change(function(){ 
				location.href="{{ url('terapi/check/') }}/" + this.value; 
			})

			$('.input').change(function(){
				if(window.terapi==null)
					return;

				var tid = window.terapi.id;
				var field = $(this).attr('name');
				var value = $(this).val();

				if(field!='jenis'&&field!='description')
					return;

				$.post('{{ url('api/save-terapi') }}',{tid:tid,field:field,value:value,_token:'{{ csrf_token() }}'},function(res){
					console.log(res.result);
				});	

			
			});

		}

		var saverow = function(th){
			if(window.terapi==null)
				return;

			var x = $('.table-input').find('tbody').find('.btn-success').index(th);
			if(!isNaN(parseInt(x))){
				
				var id = $('input[name=id]').eq(x).val();
				var tid = window.terapi.id;
				var program = $('textarea[name=program]').eq(x).val();
				var aktivitas = $('textarea[name=aktivitas]').eq(x).val();
				var keterangan = $('textarea[name=keterangan]').eq(x).val();
				$.post('{{ url('api/save-terapi-item') }}',{id:id,tid:tid,program:program,aktivitas:aktivitas,keterangan:keterangan,_token:'{{ csrf_token() }}'},function(res){
					location.reload();
				});	

			}
		}

		var deleterow = function(th){
			if(window.terapi==null)
				return;

			if(!confirm('Yakin akan dihapus?'))
				return;

			var x = $('.table-input').find('tbody').find('.btn-danger').index(th);
			if(!isNaN(parseInt(x))){
				
				var id = $('input[name=id]').eq(x).val();
				$.post('{{ url('api/delete-terapi-item') }}',{id:id,_token:'{{ csrf_token() }}'},function(res){
					location.reload();
				});					
			}
		} 


		$(document).ready(function(){

			if(window.terapi!=null){
				$.get('{{ url('api/get-all-terapi') }}',{tid:window.terapi.id},function(res){
					if(res.result){
						
						$(res.data).each(function(a,b){
							$('.table-input').find('tbody').append('<tr>'
									+ '<td>'
									+ ' <input type="hidden" name="id" value="' +b.id+ '" />'
									+ '	<textarea class="form-control input" name="program" >' +b.program+ '</textarea>'
									+ '</td>'
									+ '<td>'
									+ '	<textarea class="form-control input" name="aktivitas" >' +b.aktivitas+ '</textarea>'
									+ '</td>'
									+ '<td>'
									+ '	<textarea class="form-control input" name="keterangan" >' +b.keterangan+ '</textarea>'
									+ '</td>'
									+ '<td align="center">'
									+ '	<button class="btn btn-xs btn-success" title="Simpan data" onclick="saverow(this)"> <span class="fa fa-save"></span> </button><br/>'
									+ '	<button class="btn btn-xs btn-danger mt" title="Hapus data" onclick="deleterow(this)"> <span class="fa fa-remove"></span> </button>'
									+ '</td>'
									+ '</tr>');
						});

					}
					$('.table-input').find('tbody').append('<tr>'
						+ '<td>'
						+ ' <input type="hidden" name="id" />'
						+ '	<textarea class="form-control input" name="program" data-tdid=""></textarea>'
						+ '</td>'
						+ '<td>'
						+ '	<textarea class="form-control input" name="aktivitas" data-tdid=""></textarea>'
						+ '</td>'
						+ '<td>'
						+ '	<textarea class="form-control input" name="keterangan" data-tdid=""></textarea>'
						+ '</td>'
						+ '<td align="center">'
						+ '	<button class="btn btn-xs btn-success" title="Simpan data" onclick="saverow(this)"> <span class="fa fa-save"></span> </button><br/>'
						+ '	<button class="btn btn-xs btn-danger mt" title="Hapus data" onclick="deleterow(this)" disabled> <span class="fa fa-remove"></span> </button>'
						+ '</td>'
						+ '</tr>');
				});
			}


			// console.log(cc);

			@if(!empty($terapi))
				@if(Gen::compare_date($terapi->tanggal,date('Y-m-d'))==-1 || $terapi->pasien->status!='assessment')
			$('textarea.input').attr('disabled','disabled');
				@endif	
			@else
			$('textarea.input').attr('disabled','disabled');
			@endif	

			setTimeout(function(){
				activateAction();
			},1000);

		});

	</script>


@endsection