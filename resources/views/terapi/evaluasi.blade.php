@extends('welcome')

@section('content')

	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">Evaluasi</h1>
        </div>
	</div>

	<div class="row" style="min-height:600px;">

		<div class="col-lg-12" style="margin-bottom: 60px">
			<label class="col-md-1 col-md-offset-3">Pasien</label>
			<div class="col-md-5">
				<select class="form-control" name="pasien">
					<option value="0"></option>
					@foreach($pasiens as $pa)
					<option value="{{ $pa->id }}" <?php if(!empty($select)) echo ($pa->id==$select->id) ? 'selected':''; ?> >{{ $pa->nama_lengkap }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="col-md-12 text-center form-horizontal">
			<h4>Evaluasi 8 Bulan</h4>
			
			<div class="form-group mt">
				<label class="col-md-3 col-md-offset-2 text-left">Nama</label>
				<div class="col-md-5">
					<input type="text" name="nama" class="form-control" placeholder="Nama" value="<?= empty($select) ? '':$select->nama_lengkap ?>" disabled />
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-md-offset-2 text-left mt">Tempat, Tanggal lahir</label>
				<div class="col-md-2">
					<input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" value="<?= empty($select) ? '':$select->tempat_lahir ?>" disabled/>
				</div>
				<div class="col-md-3 ">
					<input type="text" name="tanggal_lahir" class="form-control" placeholder="Tanggal Lahir" value="<?= empty($select) ? '':Gen::human_date($select->tanggal_lahir) ?>" disabled/>
				</div>
			</div>
			<div class="form-group">
				<label class="col-md-3 col-md-offset-2 text-left">Usia</label>
				<div class="col-md-2">
					<div class="input-group">
						<input type="text" name="usia" class="form-control" placeholder="Usia"  value="<?= empty($select) ? '':Gen::calc_age($select->tanggal_lahir) ?>" disabled />
						<span class="input-group-addon">Tahun</span>
					</div>
				</div>
			</div>


			<hr/>

			<table class="table table-bordered mt">
				<thead>
					<tr>
						<th>No</th>
						<th>Jenis Terapi</th>
						<th>Tanggal Mengisi</th>
						<th>Tanggal Update</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($evaluasi as $a=>$ev)
					<tr>
						<td>{{ $a+1 }}</td>
						<td>{{ $ev->jenis_terapi }}</td>
						<td>{{ $ev->created_at }}</td>
						<td>{{ $ev->updated_at }}</td>
						<td>
							<a class="btn btn-xs btn-success" href="{{ url('evaluasi/' . $ev->pasien_id .'/'. $ev->id) }}">Tampil</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

			@if(!empty($select))
			<a class="btn btn-primary" href="{{ url('evaluasi/' . $select->id) }}" >Tambah</a>
			@endif

			<hr/>

			@if(!empty($evaluasi_select))
			<a href="{{ url('evaluasi-output/' . $idp .'/'. $ide) }}" class="btn btn-lg btn-warning" title="Output"> <span class="fa fa-file"></span> Output Evaluasi</a>
			@endif

			<form method="post" action="{{ url('api/save-evaluasi') }}">
				{{ csrf_field() }}
				<input type="hidden" name="id" value="{{ empty($evaluasi_select) ? '':$evaluasi_select->id }}" />
				<input type="hidden" name="pasien_id" value="{{ empty($select) ? '':$select->id }}" />
				<div class="form-group mt">
					<label class="col-md-3 col-md-offset-2 text-left">Jenis Terapi</label>
					<div class="col-md-4">
						<input type="text" name="jenis_terapi" class="form-control" placeholder="Jenis Terapi" value="{{ empty($evaluasi_select) ? '':$evaluasi_select->jenis_terapi }}" />
					</div>
				</div>
				<div class="form-group mt">
					<label class="col-md-2 col-md-offset-1 text-left">Karakteristik</label>
					<div class="col-md-8">
						<textarea name="karakteristik" class="form-control" rows="10" >{{ empty($evaluasi_select) ? '':$evaluasi_select->karakteristik }}</textarea>
					</div>
				</div>
				<div class="form-group mt">
					<label class="col-md-2 col-md-offset-1 text-left">Program Intervensi Terpadu</label>
					<div class="col-md-8">
						<textarea name="program" class="form-control" rows="10" >{{ empty($evaluasi_select) ? '':$evaluasi_select->program }}</textarea>
					</div>
				</div>
				<div class="form-group mt">
					<label class="col-md-2 col-md-offset-1 text-left">Tujuan</label>
					<div class="col-md-8">
						<textarea name="tujuan" class="form-control" rows="10" >{{ empty($evaluasi_select) ? '':$evaluasi_select->tujuan }}</textarea>
					</div>
				</div>
				<div class="form-group mt">
					<label class="col-md-2 col-md-offset-1 text-left">Aktivitas</label>
					<div class="col-md-8">
						<textarea name="aktivitas" class="form-control" rows="10" >{{ empty($evaluasi_select) ? '':$evaluasi_select->aktivitas }}</textarea>
					</div>
				</div>
				<div class="form-group mt">
					<label class="col-md-2 col-md-offset-1 text-left">Evaluasi</label>
					<div class="col-md-8">
						<textarea name="evaluasi" class="form-control" rows="10" >{{ empty($evaluasi_select) ? '':$evaluasi_select->evaluasi }}</textarea>
					</div>
				</div>
				<div class="form-group mt">
					<div class="col-md-1 col-md-offset-1">
						<button class="btn btn-lg btn-primary"><span class="fa fa-save"></span> Simpan </button>
					</div>
				</div>
			</form>
	</div>

	<script type="text/javascript">

		$('select[name=pasien]').chosen();

		$('select[name=pasien]').change(function(){ 
			location.href="{{ url('evaluasi/') }}/" + this.value; 
		});

		CKEDITOR.replace('karakteristik', {});
		CKEDITOR.replace('program', {});
		CKEDITOR.replace('tujuan', {});
		CKEDITOR.replace('aktivitas', {});
		CKEDITOR.replace('evaluasi', {});

	</script>


@endsection