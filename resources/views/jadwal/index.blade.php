@extends('welcome')

@section('content')

	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">Jadwal</h1>
        </div>
	</div>

	<div class="row" style="min-height:600px;">
		<div class="col-md-12">

			<div class="col-md-12">
				<h3 class="ctitle"></h3>
				<div class="pull-right form-inline">
					<div class="btn-group">
						<button class="btn btn-primary" data-calendar-nav="prev"> <span class="fa fa-arrow-left"></span> Prev</button>
						<button class="btn" data-calendar-nav="today">Today</button>
						<button class="btn btn-primary" data-calendar-nav="next">Next <span class="fa fa-arrow-right"></span></button>
					</div>
					<div class="btn-group">
						<button class="btn btn-warning" data-calendar-view="year">Year</button>
						<button class="btn btn-warning active" data-calendar-view="month">Month</button>
						<button class="btn btn-warning" data-calendar-view="week">Week</button>
						<button class="btn btn-warning" data-calendar-view="day">Day</button>
					</div>
				</div>
			</div>

			<br/>

			<div class="col-md-12" style="padding-top: 20px; padding-bottom: 50px;">
				<div id="calendar"></div>
			</div>	
			

		</div>
	</div>

	<style type="text/css">
		.event-assessment{
			background-color: green;
		}
		.event-terapi{
			background-color: blue;
		}
	</style>


	<script type="text/javascript">
		var opsi = {
			language: 'id-ID',
			events_source: '{{ url('jadwal/daftar') }}',
			view: 'month',
			tmpl_path: 'tmpls/',
			tmpl_cache: false,
			onAfterEventsLoad: function(events) {
				if(!events) {
					return;
				}
				var list = $('#eventlist');
				list.html('');

				$.each(events, function(key, val) {
					$(document.createElement('li'))
						.html('<a href="' + val.url + '">' + val.title + '</a>')
						.appendTo(list);
				});
			},
			onAfterViewLoad: function(view) {
				$('.ctitle').text(this.getTitle());
				$('.btn-group button').removeClass('active');
				$('button[data-calendar-view="' + view + '"]').addClass('active');
			},
			classes: {
				months: {
					general: 'label'
				}
			}
		};

		var calendar = $("#calendar").calendar(opsi);

		$('.btn-group button[data-calendar-nav]').each(function() {
			var $this = $(this);
			$this.click(function() {
				calendar.navigate($this.data('calendar-nav'));
			});
		});

		$('.btn-group button[data-calendar-view]').each(function() {
			var $this = $(this);
			$this.click(function() {
				calendar.view($this.data('calendar-view'));
			});
		});
	</script>

@endsection