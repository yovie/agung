@extends('welcome')

@section('content')

	<div class="row">
    	<div class="col-lg-12">
        	<h1 class="page-header">Pengguna</h1>
        </div>
	</div>

	<div class="row">

		<table class="table table-bordered">
			<thead>
				<tr>
					<th width="10px">No</th>
					<th>Nama</th>
					<th>Email</th>
					<th>Status</th>
					<th>Group</th>
					<th width="100px"></th>
				</tr>
			</thead>
			<tbody>
				@foreach($pengguna as $dd=>$ee)
				<?php 
					unset($ee->password);
				?>
				<tr>
					<td>{{ $dd+1 }}</td>
					<td>{{ $ee->name }}</td>
					<td>{{ $ee->email }}</td>
					<td align="center"><?= $ee->active==1 ? '<font color="green">Aktif</font>':'<font color="red">Tidak Aktif</font>' ?></td>
					<td align="center">{{ $ee->group->name }}</td>
					<td align="center">
						<button class="btn btn-xs btn-warning" title="edit" data-id="{{ $dd }}" data-toggle="modal" data-target="#exampleModal"> <span class="fa fa-edit"></span> </button>
						<button class="btn btn-xs btn-default" title="disable" data-id="{{ $dd }}" onclick="disable(this)" > <span class="fa fa-hand-paper-o"></span></button>
						<button class="btn btn-xs btn-danger" title="delete" data-id="{{ $dd }}" onclick="remove(this)" > <span class="fa fa-remove"></span></button>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>

		<br/>

		<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#exampleModal">Tambah Pengguna</button>

	</div>


	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h3 class="modal-title" id="exampleModalLabel">Pengguna</h3>
	      </div>
	      <div class="modal-body">
	        
	        <form class="form-horizontal" action="{{ url('pengguna/save') }}" method="post">
	        	
	        	{{ csrf_field() }}

	        	<input type="hidden" name="id" />

	        	<div class="form-group">
	        		<label class="col-md-3">Nama</label>
	        		<div class="col-md-7">
	        			<input type="text" name="name" class="form-control" required="true"/>
	        		</div>
	        	</div>
	        	<div class="form-group">
	        		<label class="col-md-3">Email</label>
	        		<div class="col-md-5">
	        			<input type="email" name="email" class="form-control" required="true"/>
	        		</div>
	        	</div>
	        	<div class="form-group">
	        		<label class="col-md-3">Password</label>
	        		<div class="col-md-5">
	        			<input type="password" name="password" class="form-control" required="true"/>
	        		</div>
	        	</div>
	        	<div class="form-group">
	        		<label class="col-md-3">Password Lagi</label>
	        		<div class="col-md-5">
	        			<input type="password" name="password1" class="form-control" required="true"/>
	        		</div>
	        	</div>
	        	<div class="form-group">
	        		<label class="col-md-3">Group</label>
	        		<div class="col-md-5">
	        			<select name="group_id" class="form-control" required="true">
	        				<option></option>
	        				<option value="1">ADMIN</option>
	        				<option value="2">REGISTER</option>
	        				<option value="3">ASSESSMENT</option>
	        				<option value="4">THERAPIST</option>
	        			</select>
	        		</div>
	        	</div>
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> &nbsp; 
		        <button type="submit" class="btn btn-primary">Save changes</button>
	        </form>

	      </div>
	    </div>
	  </div>
	</div>


	<script type="text/javascript">

		window.data = <?= json_encode($pengguna) ?>;
		
		$('table').dataTable();


		$('#exampleModal').on('shown.bs.modal', function (e) {
		    var b = $(e.relatedTarget);
		    var id = b.data('id');
		    var mm = window.data[id];
		    
		    $('input[name=id]').val('');
		    $('input[name=name]').val('');
		    $('input[name=email]').val('');
		    $('input[name=password]').val('');
		    $('input[name=password1]').val('');
		    $('select[name=group_id]').val('');

		    if(mm!=undefined){
				$('input[name=id]').val(mm.id);
			    $('input[name=name]').val(mm.name);
			    $('input[name=email]').val(mm.email);
			    $('select[name=group_id]').val(mm.group_id);		    	
		    }
		} );


		function disable(th){
			var m = $(th).data('id');
			var id = window.data[m].id;
			var status = window.data[m].active;
			status = (status=='1') ? '0':'1';
			if(id==undefined)
				return;
			$.post('{{ url('pengguna/status') }}',{id:id,enable:status,_token:'{{ csrf_token() }}'},function(res){
				location.reload();
			});
		}

		function remove(th){
			var m = $(th).data('id');
			var id = window.data[m].id;
			if(id==undefined)
				return;
			if(!confirm('Yakin akan dihapus ?'))
				return;
			$.post('{{ url('pengguna/delete') }}',{id:id,_token:'{{ csrf_token() }}'},function(res){
				location.reload();
			});
		}

	</script>

@endsection