<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//~ Route::get('/', function () {
    //~ return view('welcome');
//~ });

Route::get('/', 'PublicController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'pengguna'], function () {
    
    Route::get('/', 'UserController@index')->middleware('can:user-management');
    Route::post('/save', 'UserController@add')->middleware('can:user-management');
    Route::post('/status', 'UserController@status')->middleware('can:user-management');
    Route::post('/delete', 'UserController@delete')->middleware('can:user-management');
    
});

Route::group(['prefix' => 'jadwal'], function () {
    
    Route::get('/', 'JadwalController@index')->middleware('can:jadwal');
    Route::get('/daftar', 'JadwalController@jadwal')->middleware('auth');
    
});

Route::group(['prefix' => 'registrasi'], function () {
    
    Route::get('/', 'RegistrasiController@index')->middleware('can:registrasi')->name('registrasi');
    Route::get('/baru/{idp?}', 'RegistrasiController@add')->middleware('can:registrasi');
    Route::post('/simpan', 'RegistrasiController@save')->middleware('can:registrasi');
    Route::post('/hapus', 'RegistrasiController@delete')->middleware('can:registrasi');
    Route::get('/{idp?}', 'RegistrasiController@view')->middleware('can:registrasi');
    Route::get('/output/{idp?}', 'RegistrasiController@out')->middleware('can:registrasi');
    
});

Route::group(['prefix' => 'assessment'], function () {
    
    Route::get('/', 'AssessmentController@index')->middleware('can:assessment');
    Route::get('/form', 'AssessmentController@form')->middleware('can:assessment');
    Route::get('/form/{ida?}', 'AssessmentController@form')->middleware('can:assessment')->name('formassessment');
    Route::get('/check/{idp?}', 'AssessmentController@reg_assessment')->middleware('can:assessment');
    Route::get('/output/{ida?}', 'AssessmentController@out')->middleware('can:assessment');
    
});

Route::group(['prefix' => 'terapi'], function () {
    
    Route::get('/', 'TerapiController@index')->middleware('can:terapi')->name('daftar-terapi');
    Route::get('/form', 'TerapiController@form')->middleware('can:terapi');
    Route::get('/form/{ida?}', 'TerapiController@form')->middleware('can:terapi')->name('formterapi');
    Route::get('/check/{idp?}', 'TerapiController@reg_terapi')->middleware('can:terapi');
    Route::get('/output/{ida?}', 'TerapiController@out')->middleware('can:terapi');
    
});

Route::get('/profile', 'SettingsController@profile')->middleware('auth')->name('profile');
Route::post('/profile', 'SettingsController@update')->middleware('auth');

Route::post('/assessment', 'SettingsController@assessment_save')->middleware('can:user-management');
Route::post('/assessment-hapus', 'SettingsController@assessment_delete')->middleware('can:user-management');

Route::post('/terapi', 'SettingsController@terapi_save')->middleware('can:user-management');
Route::post('/terapi-hapus', 'SettingsController@terapi_delete')->middleware('can:user-management');

Route::get('/petugas-assessment', 'SettingsController@assessment')->middleware('can:user-management')->name('assessment');
Route::get('/petugas-terapi', 'SettingsController@therapist')->middleware('can:user-management')->name('terapi');


Route::get('/evaluasi', 'TerapiController@evaluasi')->middleware('can:terapi')->name('evaluasi');
Route::get('/evaluasi/{idp?}', 'TerapiController@evaluasi')->middleware('can:terapi');
Route::get('/evaluasi/{idp?}/{ide?}', 'TerapiController@evaluasi')->middleware('can:terapi');
Route::get('/evaluasi-output/{idp?}/{ide?}', 'TerapiController@evaluasi_out')->middleware('can:terapi');



Route::get('/api/test', 'ApiController@test')->middleware('auth');
Route::post('/api/save-field', 'ApiController@save_field')->middleware('auth');
Route::post('/api/save-assessment', 'ApiController@save_assessment')->middleware('auth');
Route::post('/api/close-assessment', 'ApiController@close_assessment')->middleware('auth');
Route::get('/api/get-assessment', 'ApiController@get_assessment')->middleware('auth');
Route::post('/api/save-assessment-info', 'ApiController@save_assessment_info')->middleware('auth');
Route::get('/api/get-assessment-info', 'ApiController@get_assessment_info')->middleware('auth');

Route::get('/api/get-all-terapi', 'ApiController@get_all_terapi')->middleware('auth');
Route::post('/api/save-terapi-item', 'ApiController@save_terapi_item')->middleware('auth');
Route::post('/api/save-terapi', 'ApiController@save_terapi')->middleware('auth');
Route::post('/api/delete-terapi-item', 'ApiController@delete_terapi_item')->middleware('auth');

Route::post('/api/save-evaluasi', 'ApiController@save_evaluasi')->middleware('auth');


