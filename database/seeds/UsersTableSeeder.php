<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id' => 1,
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => Hash::make('admin'),
                'active' => 1,
                'group_id' => 1
            ],
            [
                'id' => 2,
                'name' => 'register',
                'email' => 'register@gmail.com',
                'password' => Hash::make('register'),
                'active' => 1,
                'group_id' => 2
            ],
            [
                'id' => 3,
                'name' => 'assessment',
                'email' => 'assessment@gmail.com',
                'password' => Hash::make('assessment'),
                'active' => 1,
                'group_id' => 3
            ],
            [
                'id' => 4,
                'name' => 'therapist',
                'email' => 'therapist@gmail.com',
                'password' => Hash::make('therapist'),
                'active' => 1,
                'group_id' => 4
            ]
        ]);
    }
}
