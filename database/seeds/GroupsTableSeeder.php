<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->insert([
            [
                'id' => 1,
                'name' => 'ADMIN',
                'description' => 'Administrator',
            ],
            [
                'id' => 2,
                'name' => 'REGISTER',
                'description' => 'Registrasi siswa',
            ],
            [
                'id' => 3,
                'name' => 'ASSESSMENT',
                'description' => 'Assessment siswa',
            ],
            [
                'id' => 4,
                'name' => 'THERAPIST',
                'description' => 'Terapis',
            ]
        ]);
    }
}
