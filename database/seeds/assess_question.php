<?php

use Illuminate\Database\Seeder;

class assess_question extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('assessment_questions')->insert([
            [
                'id' => null,
                'code' => 'A',
                'question' => 'Apakah masa kehamilan ibunda bermasalah? (Bermasalah/ Tidak bermasalah)'
            ],
            [
                'id' => null,
                'code' => 'A1',
                'question' => 'Bermasalah, ceritakan permasalahannya'
            ],
            [
                'id' => null,
                'code' => 'B',
                'question' => 'Apakah kehamilan ini kehamilan yang diinginkan?'
            ],
            [
                'id' => null,
                'code' => 'C',
                'question' => 'Riwayat kehamilan'
            ],
            [
                'id' => null,
                'code' => 'C1',
                'question' => 'Ibu hamil saat usia?'
            ],
            [
                'id' => null,
                'code' => 'C2',
                'question' => 'Apakah ibu mengalami sakit saat hamil?'
            ],
            [
                'id' => null,
                'code' => 'C3',
                'question' => 'Apakah ibu mengalami trauma saat masa kehamilan?'
            ],
            [
                'id' => null,
                'code' => 'C4',
                'question' => 'Apakah ibu mengkonsumsi obat-obatan atau jamu?'
            ],
            [
                'id' => null,
                'code' => 'D',
                'question' => 'Riwayat kelahiran'
            ],
            [
                'id' => null,
                'code' => 'D1',
                'question' => 'Bagaimana proses kelahiran anak? Normal/ Caesar/ Menggunakan alat bantu?'
            ],
            [
                'id' => null,
                'code' => 'D2',
                'question' => 'Apakah anak langsung menangis saat lahir?'
            ],
            [
                'id' => null,
                'code' => 'D3',
                'question' => 'Apakah anak kuning saat lahir?'
            ],
            [
                'id' => null,
                'code' => 'D4',
                'question' => 'Berat dan panjang anak saat lahir?'
            ],
            [
                'id' => null,
                'code' => 'D5',
                'question' => 'Apakah ada hal penting lain yang perlu diketahui tentang anak?'
            ],
            [
                'id' => null,
                'code' => 'E',
                'question' => 'Riwayat perkembangan bahasa'
            ],
            [
                'id' => null,
                'code' => 'E1',
                'question' => 'Mengoceh, usia :'
            ],
            [
                'id' => null,
                'code' => 'E2',
                'question' => 'Kata-kata pertama yang muncul, usia :'
            ],
            [
                'id' => null,
                'code' => 'E3',
                'question' => 'Kalimat dengan 2 kata, usia :'
            ],
            [
                'id' => null,
                'code' => 'E4',
                'question' => 'Pengucapan jelas atau tidak jelas, usia :'
            ],
            [
                'id' => null,
                'code' => 'E5',
                'question' => 'Mengucapkan kalimat sederhana (Mampu/ Belum mampu), usia :'
            ],
            [
                'id' => null,
                'code' => 'E6',
                'question' => 'Gagap atau tidak, usia :'
            ],
            [
                'id' => null,
                'code' => 'F',
                'question' => 'Adakah keluarga lain dalam keluarga inti yang tinggal dengan anak dalam satu rumah?'
            ],
            [
                'id' => null,
                'code' => 'G',
                'question' => 'Apakah anak bermain dengan orang tua?'
            ],
            [
                'id' => null,
                'code' => 'H',
                'question' => 'Bagaimana perilaku di rumah? Aktif, pasif, sulit diatur, mudah diatur, mau bermain dengan saudara atau tidak?'
            ],
            [
                'id' => null,
                'code' => 'I',
                'question' => 'Masalah perilaku lain di rumah, jelaskan (ketika marahdan ketika senang)'
            ]
        ]);
    }
}
