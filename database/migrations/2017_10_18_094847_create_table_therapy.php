<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTherapy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('therapies', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal')->nullable();
            $table->unsignedInteger('assessment_id')->nullable();
            $table->unsignedInteger('pasien_id')->nullable();
            $table->unsignedInteger('petugas_id')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();

            $table->char('jenis',30)->nullable();
            
            $table->string('description')->nullable();
            
            $table->foreign('pasien_id')
                ->references('id')->on('pasiens')
                ->onDelete('cascade');
            $table->foreign('petugas_id')
                ->references('id')->on('petugas')
                ->onDelete('cascade');
            $table->foreign('assessment_id')
                ->references('id')->on('assessments')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('therapies');
    }
}
