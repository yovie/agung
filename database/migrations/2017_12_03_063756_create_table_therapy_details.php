<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTherapyDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('therapy_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('therapies_id')->nullable();

            $table->char('program',255)->nullable();
            $table->string('aktivitas')->nullable();
            $table->string('keterangan')->nullable();

            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();

            $table->foreign('therapies_id')
                ->references('id')->on('therapies')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('therapy_details');
    }
}
