<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAssessment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assessments', function (Blueprint $table) {
            $table->increments('id');
            $table->date('tanggal')->nullable();
            $table->date('tanggal_terapi')->nullable();
            $table->unsignedInteger('petugas_terapi_id')->nullable();
            // $table->string('keterangan')->nullable();
            $table->unsignedInteger('pasien_id')->nullable();
            $table->unsignedInteger('petugas_id')->nullable();
            $table->string('description')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            
            $table->foreign('pasien_id')
                ->references('id')->on('pasiens')
                ->onDelete('cascade');
            $table->foreign('petugas_id')
                ->references('id')->on('petugas')
                ->onDelete('cascade');
            $table->foreign('petugas_terapi_id')
                ->references('id')->on('petugas')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assessments');
    }
}
