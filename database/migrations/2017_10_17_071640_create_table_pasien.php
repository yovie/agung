<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePasien extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasiens', function (Blueprint $table) {
            $table->increments('id');
            // $table->char('nik', 37)->unique();
            $table->date('tanggal_assessment')->nullable();
            $table->unsignedInteger('petugas_assessment')->nullable();
            $table->time('jam_assessment')->nullable();
            
            $table->char('np', 50)->nullable();
            $table->char('nama_lengkap', 50)->nullable();
            $table->char('nama_panggilan', 50)->nullable();
            $table->enum('gender', ['L','P'])->default('L');
            $table->char('tempat_lahir', 50)->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->char('agama', 50)->nullable();
            $table->char('kewarganegaraan', 50)->nullable();
            $table->integer('anak_ke')->nullable();
            $table->integer('jumlah_saudara_kandung')->nullable();
            $table->integer('jumlah_saudara_tiri')->nullable();
            $table->integer('jumlah_saudara_angkat')->nullable();
            $table->char('bahasa_sehari', 50)->nullable();
            $table->integer('berat_badan')->nullable();
            $table->integer('tinggi_badan')->nullable();
            $table->enum('golongan_darah', ['A','AB','B','O'])->nullable();
            $table->char('penyakit_berat', 50)->nullable();
            $table->char('berkebutuhan_khusus', 50)->nullable();

            $table->string('alamat')->nullable();
            $table->char('telepon', 30)->nullable();
            $table->char('rt', 10)->nullable();
            $table->char('rw', 10)->nullable();
            $table->char('nama_dusun', 50)->nullable();
            $table->char('nama_kelurahan', 50)->nullable();
            $table->char('kecamatan', 10)->nullable();
            $table->char('kode_pos', 10)->nullable();
            $table->char('model_transportasi', 10)->nullable();

            $table->char('tinggal_pada', 50)->nullable();
            $table->char('nama_ayah_kandung', 50)->nullable();
            $table->char('nama_ibu_kandung', 50)->nullable();
            $table->char('pendidikan_ayah', 50)->nullable();
            $table->char('pendidikan_ibu', 50)->nullable();
            $table->char('pekerjaan_ayah', 50)->nullable();
            $table->char('pekerjaan_ibu', 50)->nullable();
            $table->char('penghasilan_ayah', 50)->nullable();
            $table->char('penghasilan_ibu', 50)->nullable();
            $table->char('tempatlahir_ayah', 50)->nullable();
            $table->char('tempatlahir_ibu', 50)->nullable();
            $table->date('tanggallahir_ayah')->nullable();
            $table->date('tanggallahir_ibu')->nullable();
            $table->char('nama_wali', 50)->nullable();
            $table->char('pendidikan_wali', 50)->nullable();
            $table->char('hubungan_wali', 50)->nullable();
            $table->char('pekerjaan_wali', 50)->nullable();
            $table->char('penghasilan_wali', 50)->nullable();

            $table->char('masuk_sebagai', 50)->nullable();
            $table->char('asal_anak', 50)->nullable();
            $table->char('nama_tk', 50)->nullable();
            $table->char('nomor_tahun_surat', 50)->nullable();

            $table->enum('status', ['register','assessment','terapi','close'])->default('register');
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();

            $table->foreign('petugas_assessment')
                ->references('id')->on('petugas')
                ->onDelete('cascade');
            $table->foreign('created_by')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('updated_by')
                ->references('id')->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasiens');
    }
}
