<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEvaluations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('pasien_id')->nullable();
            $table->char('jenis_terapi',50)->nullable();
            
            $table->string('karakteristik')->nullable();
            $table->string('program')->nullable();
            $table->string('tujuan')->nullable();
            $table->string('aktivitas')->nullable();
            $table->string('evaluasi')->nullable();
            
            $table->dateTime('created_at')->nullable();
            $table->dateTime('updated_at')->nullable();

            $table->foreign('pasien_id')
                ->references('id')->on('pasiens')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluations');
    }
}
