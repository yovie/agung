<?php

namespace App\Helpers;

use DateTime;

class Gen
{
	public static function check_date($date_str)
	{
		if(empty($date_str))
			return false;
		$d = DateTime::createFromFormat('Y-m-d', $date_str);
    	return $d && $d->format('Y-m-d') === $date_str;
	}

	public static function human_date($date_str)
	{
		$dt = DateTime::createFromFormat('Y-m-d', $date_str);
		if(!$dt)
			$dt = DateTime::createFromFormat('Y-m-d H:i:s', $date_str);
		if($dt)
			return $dt->format('d F Y');
	}

	public static function check_time($time_str)
	{
		$b = explode(':', $time_str);
		if(count($b)!=2)
			return false;
		return ( intval($b[0])>=0 && intval($b[0])<=24 )
			&& ( intval($b[1])>=0 && intval($b[1])<=59 );
	}

	public static function human_time($time_str)
	{
		$t = explode(':', $time_str);
		if(count($t)!=3)
			return false;
		return $t[0].':'.$t[1];
	}

	public static function date_to_mls($date_str)
	{
		$dt = DateTime::createFromFormat('Y-m-d', $date_str);
		if(!$dt)
			$dt = DateTime::createFromFormat('Y-m-d H:i:s', $date_str);
		if($dt)
			return $dt->getTimestamp()*1000;	
	}

	public static function diff_days($a, $b)
	{
		$b = Gen::date_to_mls($b)/1000;
		$at = new DateTime;
		$at->setTimestamp($a);
		$bt = new DateTime;
		$bt->setTimestamp($b);
		$dt = $at->diff($bt);
		return $dt->days;
	}

	public static function to_date_time($date_str)
	{
		$dt = DateTime::createFromFormat('Y-m-d', $date_str);
		if(!$dt)
			$dt = DateTime::createFromFormat('Y-m-d H:i:s', $date_str);
		if($dt)
			return $dt;
	}

	public static function calc_age($date_birth)
	{
		$b = Gen::to_date_time($date_birth);
		$n = new DateTime('now');
		return $b->diff($n)->y;
	}

	public static function dt_now()
	{
		return date('Y-m-d H:i:s');
	}

	public static function compare_date($date_str1, $date_str2)
	{
		$d1 = Gen::to_date_time($date_str1);
		$d2 = Gen::to_date_time($date_str2);
		if($d1->getTimestamp()>$d2->getTimestamp())
			return 1;
		else if($d1->getTimestamp()<$d2->getTimestamp())
			return -1;
		else if($d1->getTimestamp()==$d2->getTimestamp())
			return 0;
		else
			return null;
	}

}
