<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class AqAnswer extends Model
{
	use Notifiable;

    protected $fillable = [
        'aq', 'qcode', 'answer', 'created_by', 'updated_by', 'created_at', 'updated_at'
    ];

    protected $hidden = [];
}
