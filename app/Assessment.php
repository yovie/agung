<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

use App\Therapy;


class Assessment extends Model
{
    use Notifiable;

    protected $fillable = ['tanggal','tanggal_terapi','description','pasien_id','petugas_id','created_at','updated_at'];

    protected $hidden = [];


	public function pasien()
    {
        return $this->belongsTo('App\Pasien', 'pasien_id');
    }    

    public function petugasAssessment()
    {
        return $this->belongsTo('App\Petugas', 'petugas_id');
    }

    public function answer()
    {
    	return $this->hasMany('App\AqAnswer', 'aq', 'id');
    }

    public function terapi()
    {
        $terapi = Therapy::where('assessment_id',$this->id)->get()->first();
        return $terapi;
    }
}
