<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Petugas extends Model
{
	use Notifiable;

    protected $fillable = [
        'nip', 'nama', 'hp', 'email', 'jenis', 'created_at', 'updated_at'
    ];

    protected $hidden = [];
}
