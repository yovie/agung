<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Evaluation extends Model
{

    use Notifiable;

    protected $fillable = ['pasien_id','jenis_terapi','karakteristik','program','tujuan','aktivitas','evaluasi','created_at','updated_at'];

    protected $hidden = [];

}
