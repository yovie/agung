<?php

namespace App\Http\Controllers;

use Auth;
use Gen;
use App\Refs;
use App\Petugas;
use App\Pasien;
use App\AssessmentQuestion;
use App\AqAnswer;
use App\User;
use App\Assessment;
use App\Therapy;
use App\TherapyDetail;
use App\Evaluation;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function __construct()
    {
        $this->refs = new Refs;
    }

    public function test()
    {
    	return [
    		'result' => true,
    		'message' => 'Success'
    	];
    }


    public function save_field(Request $request)
    {
    	$table = $request->table;
    	$field = $request->field;
    	$value = $request->value;
    	$key = $request->key;

    	$model = null;

    	if($table=='Pasien')
    		$model = Pasien::find($key);
    	if($table=='Petugas')
    		$model = Petugas::find($key);
    	if($table=='AssessmentQuestion')
    		$model = AssessmentQuestion::find($key);

    	$model->{$field} = $value;

    	$model->save();
    }

    public function save_assessment(Request $request)
    {
    	$aq = $request->aq;
    	$qcode = $request->qcode;
    	$answer = $request->answer;
    	$m = AqAnswer::where('aq',$aq)->where('qcode',$qcode)->get()->first();
    	if(!empty($m))
    	{
            $m->answer = $answer;
	    	$m->updated_by = Auth::user()->id;
	    	$m->save();
	    }
	    return [
	    	'result'=>true,
	    	'answer'=>$m->answer
	    ];
    }

    public function close_assessment(Request $request)
    {
        $ida = $request->ida;
        $m = Assessment::find($ida);
        if($m){
            $pas = Pasien::find($m->pasien_id);
            if($pas){
                $pas->status = 'assessment'; 
                $pas->save();   
                return [
                    'result'=>true
                ];   
            }    
        }
        return [
            'result'=>false
        ];   
    }

    public function get_assessment(Request $request)
    {
    	$aq = $request->aq;
    	$qcode = $request->qcode;
    	$m = AqAnswer::where('aq',$aq)->where('qcode',$qcode)->get()->first();
    	if(empty($m))
    	{
    		$m = new AqAnswer;
    		$m->aq = $aq;
    		$m->qcode = $qcode;
    		$m->save();
    	}
    	return [
    		'result'=>true,
    		'answer'=>$m->answer
    	];

    }

    public function save_assessment_info(Request $request)
    {
        $aid = $request->aid;
        $field = $request->field;
        $value = $request->value;
        $m = Assessment::where('id',$aid)->get()->first();
        if(!empty($m))
        {
            $m->{$field} = $value;
            $m->updated_at = Gen::dt_now();
            $m->save();
        }
        return [
            'result'=>true,
            'value'=>$m->{$field}
        ];
    }

    public function get_assessment_info(Request $request)
    {
        $aid = $request->aid;
        $field = $request->field;
        $m = Assessment::where('id',$aid)->get()->first();
        if(!empty($m))
        {
            return [
                'result'=>true,
                'value'=>$m->{$field}
            ];
        }
        else
        {
            return [
                'result'=>false
            ];
        }
    }

    public function get_all_terapi(Request $request)
    {
        $tid = $request->tid;
        $m = TherapyDetail::where('therapies_id',$tid)->get();
        if(empty($m))
        {
            return [
                'result'=>false
            ];
        }
        else
        {
            return [
                'result'=>true,
                'data'=>$m
            ];
        }

    }

    public function save_terapi_item(Request $request)
    {
        $id = $request->id;
        $tid = $request->tid;
        $program = $request->program;
        $aktivitas = $request->aktivitas;
        $keterangan = $request->keterangan;
        $m = null;
        if(empty($id))
        {
            $m = new TherapyDetail;
        }
        else
        {
            $m = TherapyDetail::find($id);
            
        }

        $m->therapies_id = $tid;
        $m->program = $program;
        $m->aktivitas = $aktivitas;
        $m->keterangan = $keterangan;
        $m->save();

        return [
            'result'=>true,
            'data'=>$m
        ];
    }

    public function save_terapi(Request $request)
    {
        $tid = $request->tid;
        $field = $request->field;
        $value = $request->value;

        $m = Therapy::find($tid);
        $m->{$field} = $value;
        $m->save();

        return [
            'result'=>true
        ];
    }


    public function delete_terapi_item(Request $request)
    {
        $id = $request->id;
        if(empty($id))
        {
            return [
                'result'=>false
            ];
        }
        else
        {
            $m = TherapyDetail::find($id);
            $m->delete();
            return [
                'result'=>true
            ];
            
        }
    }

    public function save_evaluasi(Request $request)
    {
        $id = $request->id;
        $pasien_id = $request->pasien_id;
        $jenis_terapi = $request->jenis_terapi;
        $karakteristik = $request->karakteristik;
        $program = $request->program;
        $tujuan = $request->tujuan;
        $aktivitas = $request->aktivitas;
        $evaluasi = $request->evaluasi;

        if(empty($id))
            $m = new Evaluation;
        else
            $m = Evaluation::find($id);

        $m->pasien_id = $pasien_id;
        $m->jenis_terapi = $jenis_terapi;
        $m->karakteristik = $karakteristik;
        $m->program = $program;
        $m->tujuan = $tujuan;
        $m->aktivitas = $aktivitas;
        $m->evaluasi = $evaluasi;

        $m->save();

        // return [
        //     'result'=>true
        // ];
        // return redirect()->route('evaluasi',[$pasien_id]);
        return redirect('evaluasi/' . $pasien_id);
    }


}
