<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Petugas;
use App\Pasien;
use App\Refs;
use App\Http\Controllers\Controller;
use Auth;
use Gen;

require_once __DIR__ . '/../../../vendor/autoload.php';

class RegistrasiController extends Controller
{
    public function __construct()
    {
        $this->refs = new Refs;
    }

    public function index()
    {
    	$pasiens = Pasien::all();
    	return view('registrasi.index')->with('pasiens', $pasiens);
    }

    public function add($idp = null)
    {
        $edit = null;
        $petugas = Petugas::where('jenis','=','assessment')->get();

        if($idp!=null)
        {
            $edit = Pasien::find($idp);

            if(!$edit)
                abort(404, 'Invalid Request');
        }
        else
        {
            $edit = session()->get('edit');
        }

    	return view('registrasi.add')
            ->with('list_agama', $this->refs->agama())
            ->with('list_kewarganegaraan', $this->refs->kewarganegaraan())
            ->with('list_goldar', $this->refs->goldar())
            ->with('list_petugas', $petugas)
            ->with('edit', $edit)
            ->with('list_pendidikan', $this->refs->pendidikan());	
    }

    public function save(Request $request)
    {
        $pasien = null;

        if(isset($request->idp) && !empty($request->idp))
            $pasien = Pasien::find($request->idp);
        else
    	    $pasien = new Pasien;

	    if(Gen::check_date($request->tanggal_assessment))
            $pasien->tanggal_assessment = $request->tanggal_assessment;
        $pasien->petugas_assessment = $request->petugas_assessment;
        if(Gen::check_time($request->jam_assessment))
            $pasien->jam_assessment = $request->jam_assessment;
            
        $pasien->nama_lengkap = $request->nama_lengkap;
        $pasien->nama_panggilan = $request->nama_panggilan;
        $pasien->gender = $request->gender;
        $pasien->tempat_lahir = $request->tempat_lahir;
        if(Gen::check_date($request->tanggal_lahir))
            $pasien->tanggal_lahir = $request->tanggal_lahir;
        $pasien->agama = $request->agama;
        $pasien->kewarganegaraan = $request->kewarganegaraan;
        $pasien->anak_ke = $request->anak_ke;
        $pasien->jumlah_saudara_kandung = $request->jumlah_saudara_kandung;
        $pasien->jumlah_saudara_tiri = $request->jumlah_saudara_tiri;
        $pasien->jumlah_saudara_angkat = $request->jumlah_saudara_angkat;
        $pasien->bahasa_sehari = $request->bahasa_sehari;
        $pasien->berat_badan = $request->berat_badan;
        $pasien->tinggi_badan = $request->tinggi_badan;
        $pasien->golongan_darah = $request->golongan_darah;
        $pasien->penyakit_berat = $request->penyakit_berat;
        $pasien->berkebutuhan_khusus = $request->berkebutuhan_khusus;
        $pasien->alamat = $request->alamat;
        $pasien->telepon = $request->telepon;
        $pasien->rt = $request->rt;
        $pasien->rw = $request->rw;
        $pasien->nama_dusun = $request->nama_dusun;
        $pasien->nama_kelurahan = $request->nama_kelurahan;
        $pasien->kecamatan = $request->kecamatan;
        $pasien->kode_pos = $request->kode_pos;
        $pasien->model_transportasi = $request->model_transportasi;
        $pasien->tinggal_pada = $request->tinggal_pada;
        $pasien->nama_ayah_kandung = $request->nama_ayah_kandung;
        $pasien->nama_ibu_kandung = $request->nama_ibu_kandung;
        $pasien->pendidikan_ayah = $request->pendidikan_ayah;
        $pasien->pendidikan_ibu = $request->pendidikan_ibu;
        $pasien->pekerjaan_ayah = $request->pekerjaan_ayah;
        $pasien->pekerjaan_ibu = $request->pekerjaan_ibu;
        $pasien->penghasilan_ayah = $request->penghasilan_ayah;
        $pasien->penghasilan_ibu = $request->penghasilan_ibu;
        $pasien->tempatlahir_ayah = $request->tempatlahir_ayah;
        $pasien->tempatlahir_ibu = $request->tempatlahir_ibu;
        if(Gen::check_date($request->tanggallahir_ayah))
            $pasien->tanggallahir_ayah = $request->tanggallahir_ayah;
        if(Gen::check_date($request->tanggallahir_ibu))
            $pasien->tanggallahir_ibu = $request->tanggallahir_ibu;
        $pasien->nama_wali = $request->nama_wali;
        $pasien->pendidikan_wali = $request->pendidikan_wali;
        $pasien->hubungan_wali = $request->hubungan_wali;
        $pasien->pekerjaan_wali = $request->pekerjaan_wali;
        $pasien->penghasilan_wali = $request->penghasilan_wali;
        $pasien->masuk_sebagai = $request->masuk_sebagai;
        $pasien->asal_anak = $request->asal_anak;
        $pasien->nama_tk = $request->nama_tk;
        $pasien->nomor_tahun_surat = $request->nomor_tahun_surat;

	    $pasien->status = 'register';
	    $pasien->created_by = Auth::user()->id;

        $request->session()->flash('edit', $pasien);

        $val = $request->validate([
            'nama_lengkap'=>'required',
            'nama_panggilan'=>'required',
            'gender'=>'required',
            'tempat_lahir'=>'required',
            'tanggal_lahir'=>'required',
            'nama_ayah_kandung'=>'required',
            'nama_ibu_kandung'=>'required',
            'masuk_sebagai'=>'required',
        ]);

	    $pasien->save();

	    return redirect()->route('registrasi');
    }

    public function delete(Request $request)
    {
        $idp = $request->idp;
        if($idp)
        {
            $pasien = Pasien::find($idp);
            if($pasien)
                $pasien->delete();
        }
    }

    public function view($idp = null)
    {
        $view = Pasien::find($idp);

        if(!$view)
            abort(404, 'Invalid Request');

        return view('registrasi.view')
            ->with('view', $view)
            ->with('list_agama', $this->refs->agama())
            ->with('list_kewarganegaraan', $this->refs->kewarganegaraan())
            ->with('list_goldar', $this->refs->goldar())
            ->with('list_pendidikan', $this->refs->pendidikan());   
    }

    public function out($idp)
    {
        $pasien = Pasien::find($idp);
        $mpdf = new \Mpdf\Mpdf();
        
        $data = view('registrasi.output')->with('pasien',$pasien);
        $mpdf->SetTitle('Data ' . $pasien->nama_lengkap);
        // $mpdf->Image('public/img/logometro.png', 0, 0, 210, 297, 'jpg', '', true, false);

        $mpdf->WriteHTML($data->render());
        $mpdf->Output();
        exit;
    }
}
