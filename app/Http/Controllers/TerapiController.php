<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pasien;
use App\Therapy;
use App\TherapyDetail;
use App\Evaluation;
use App\Assessment;
use Gen;
use Auth;

require_once __DIR__ . '/../../../vendor/autoload.php';

class TerapiController extends Controller
{
    public function index()
    {
    	$ass = Pasien::where('status','assessment')->get();
        $ter = Therapy::all()->sortByDesc('tanggal');
    	return view('terapi.index')
            ->with('pasiens', $ass)
    		->with('terapis', $ter);
    }

    public function form($idt=null)
    {
        $pasien = Pasien::where('status','assessment')->get();

        $terapi = Therapy::find($idt);

    	return view('terapi.form')
            ->with('pasiens',$pasien)
            ->with('terapi',$terapi);	
    }

    public function reg_terapi($ida=null)
    {
        $assessment = null;
        $terapi = null;
        if($ida)
        {
            $assessment = Assessment::find($ida);
            
            if(empty($assessment))
                return redirect()->route('daftar-terapi');

            $tgl_terapi = Gen::to_date_time($assessment->tanggal_terapi);
            $tgl_terapi = (!empty($tgl_terapi)) ? $tgl_terapi->format('Y-m-d'):date('Y-m-d');
            $terapi = Therapy::where('pasien_id',$assessment->pasien_id)
                ->where('petugas_id',$assessment->petugas_terapi_id)
                ->where('tanggal',$tgl_terapi)->get()->first();

            if(count($terapi)==0)
            {
                $terapi = new Therapy;
                $terapi->assessment_id = $assessment->id;
                $terapi->pasien_id = $assessment->pasien_id;
                $terapi->petugas_id = $assessment->petugas_terapi_id;
                $terapi->tanggal = $tgl_terapi;
                $terapi->save();
            }

            return redirect()->route('formterapi',['idt'=>$terapi->id]);
        }

        return redirect()->route('formterapi');
    }

    public function evaluasi($idp=null,$ide=null)
    {
        $pasien = Pasien::all();
        $select = null;
        $eva = [];
        $eva_select = null;
        if(!empty($idp)){
            $select = Pasien::find($idp);
            $eva = Evaluation::where('pasien_id',$idp)->get();
            if(!empty($ide)){
                $eva_select = Evaluation::find($ide);
            }
        }
        return view('terapi.evaluasi')
            ->with('idp',$idp)
            ->with('ide',$ide)
            ->with('pasiens',$pasien)
            ->with('select',$select)
            ->with('evaluasi',$eva)
            ->with('evaluasi_select',$eva_select);
    }

    public function out($idt)
    {
        $mpdf = new \Mpdf\Mpdf();

        $terapi = Therapy::find($idt);
        $det = TherapyDetail::where('therapies_id',$idt)->get();
        
        $data = view('terapi.output')
            ->with('ter',$terapi)
            ->with('det',$det);
        $mpdf->SetTitle('Home Program');

        $mpdf->WriteHTML($data->render());
        $mpdf->Output();
        exit;
    }

    public function evaluasi_out($idp=null,$ide=null)
    {
        $mpdf = new \Mpdf\Mpdf();

        $select = null;
        $eva = [];
        $eva_select = null;
        if(!empty($idp)){
            $select = Pasien::find($idp);
            $eva = Evaluation::where('pasien_id',$idp)->get();
            if(!empty($ide)){
                $eva_select = Evaluation::find($ide);
            }
        }
        
        $data = view('terapi.evaluasioutput')
            ->with('idp',$idp)
            ->with('ide',$ide)
            ->with('select',$select)
            ->with('evaluasi',$eva)
            ->with('evaluasi_select',$eva_select);

        $mpdf->SetTitle('Evaluasi');

        $mpdf->WriteHTML($data->render());
        $mpdf->Output();
        exit;
    }

}
