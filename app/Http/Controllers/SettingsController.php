<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Petugas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class SettingsController extends Controller
{

    public function profile(Request $request)
    {
        $me = User::find( Auth::user()->id );
        $mtype = $request->session()->get('type');
        $mmsg = $request->session()->get('message');

    	return view('settings.profile')
            ->with('me', $me)
            ->with('type', $mtype)
            ->with('message', $mmsg);
    }

    public function assessment()
    {
        $assess = Petugas::where('jenis','=','assessment')->get();

    	return view('settings.assessment')
            ->with('petugas', $assess);
    }

    public function therapist()
    {
        $terapist = Petugas::where('jenis','=','therapist')->get();

    	return view('settings.therapist')
            ->with('petugas', $terapist);
    }


    public function update(Request $request)
    {
        if(isset($request->email))
        {
            $request->validate([
                'name'=>'required',
                'email'=>'required'
            ]);

            $me = User::find( Auth::user()->id );
            $me->name = $request->name;
            $me->email = $request->email;
            $me->save();

            $request->session()->flash('type', 'info');
            $request->session()->flash('message', 'Nama dan email sudah diubah');
        }
        else if(isset($request->pwdlama))
        {
            $request->validate([
                'pwdlama'=>'required',
                'pwdbaru'=>'required',
                'pwdlagi'=>'required'
            ]);

            $me = User::find( Auth::user()->id );

            if(Hash::check($request->pwdlama, $me->password))
            {
                if($request->pwdbaru==$request->pwdlagi)
                {
                    $me->password = Hash::make($request->pwdbaru);
                    $me->save();   

                    $request->session()->flash('type', 'info');
                    $request->session()->flash('message', 'Password sudah diubah');
                }
                else
                {
                    $request->session()->flash('type', 'error');
                    $request->session()->flash('message', 'Password konfirmasi tidak sama');
                }
            }
            else
            {
                $request->session()->flash('type', 'error');
                $request->session()->flash('message', 'Password lama salah');   
            }
        }

        return redirect()->route('profile');
    }

    public function assessment_save(Request $request)
    {
        $petugas = null;

        if(isset($request->id) && !empty($request->id))
        {
            $petugas = Petugas::find($request->id);
            $petugas->updated_at = date('Y-m-d H:i:s');
        }
        else
        {
            $petugas = new Petugas;
            $petugas->created_at = date('Y-m-d H:i:s');
        }

        $request->validate([
            'nama'=>'required'
        ]);

        $petugas->nip = $request->nip;
        $petugas->nama = $request->nama;
        $petugas->hp = $request->hp;
        $petugas->email = $request->email;
        $petugas->jenis = 'assessment';
        $petugas->save();

        return redirect()->route('assessment');
    }

    public function assessment_delete(Request $request)
    {
        $id = $request->id;
        if($id)
        {
            $petugas = Petugas::find($id);
            if($petugas)
                $petugas->delete();
        }
    }

    public function terapi_save(Request $request)
    {
        $petugas = null;

        if(isset($request->id) && !empty($request->id))
        {
            $petugas = Petugas::find($request->id);
            $petugas->updated_at = date('Y-m-d H:i:s');
        }
        else
        {
            $petugas = new Petugas;
            $petugas->created_at = date('Y-m-d H:i:s');
        }

        $request->validate([
            'nama'=>'required'
        ]);

        $petugas->nip = $request->nip;
        $petugas->nama = $request->nama;
        $petugas->hp = $request->hp;
        $petugas->email = $request->email;
        $petugas->jenis = 'therapist';
        $petugas->save();

        return redirect()->route('terapi');
    }

    public function terapi_delete(Request $request)
    {
        $id = $request->id;
        if($id)
        {
            $petugas = Petugas::find($id);
            if($petugas)
                $petugas->delete();
        }
    }
}
