<?php

namespace App\Http\Controllers;

use Gen;
use App\Pasien;
use Illuminate\Http\Request;

class JadwalController extends Controller
{
    public function index()
    {
    	return view('jadwal.index');
    }

    public function jadwal()
    {
    	$reg = $this->_getjadwal('register');
    	$ter = $this->_getjadwal('assessment');
    	$co = array_merge($reg, $ter);

    	return response()
    		->json( array(
	    		'success' => 1,
	    		'result' => $co
	    	) );
    }


    protected function _getjadwal($status)
    {
    	$pasien = Pasien::where('status','=',$status)->get();
    	$res = [];
    	foreach ($pasien as $key => $value) {
    		$res[] = array(
    			"id" => $value->id,
		       	"title" => '<strong>ASSESSMENT</stong>, ' . $value->nama_lengkap . ' oleh ' . $value->petugasAssessment->nama,
		        "url" => url('registrasi/' . $value->id),
		        "class" => "event-assessment",
		        "start" => Gen::date_to_mls($value->tanggal_assessment),
		        "end" => Gen::date_to_mls($value->tanggal_assessment)
    		);
    	}
    	return $res;
    }
}
