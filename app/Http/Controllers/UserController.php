<?php

namespace App\Http\Controllers;

use Gate;
use Illuminate\Http\Request;
use App\UserModel;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $pengguna = UserModel::all();
        return view('user.index')
                ->with('pengguna',$pengguna);
    }

    public function view($id)
    {

    }

    public function add(Request $req)
    {
        $id = $req->id;
        $name = $req->name;
        $email = $req->email;
        $password = $req->password;
        $password1 = $req->password1;
        $group_id = $req->group_id;

        if(!filter_var($email,FILTER_VALIDATE_EMAIL))
            return redirect('pengguna');

        if(empty($password))
            return redirect('pengguna');

        if($password!=$password1)
            return redirect('pengguna');

        $pengguna = null;

        if(empty($id))
            $pengguna = new UserModel;
        else
            $pengguna = UserModel::find($id);

        $pengguna->name = $name;
        $pengguna->email = $email;
        $pengguna->password = Hash::make($password);
        $pengguna->active = 1;
        $pengguna->group_id = $group_id;
        $pengguna->save();

        return redirect('pengguna');
    }

    public function status(Request $req)
    {
        $id = $req->id;
        $enable = $req->enable;
        if(!empty($id)){
            $mm = UserModel::find($id);
            $mm->active = $enable;
            $mm->save();
        }
        return redirect('pengguna');
    }

    public function delete(Request $req)
    {
    	$id = $req->id;
        if(!empty($id)){
            $mm = UserModel::find($id);
            $mm->delete();
        }
        return redirect('pengguna');
    }
}
