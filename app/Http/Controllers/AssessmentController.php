<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Petugas;
use App\Pasien;
use App\Refs;
use App\AssessmentQuestion;
use App\Assessment;
use Gen;
use Auth;

require_once __DIR__ . '/../../../vendor/autoload.php';

class AssessmentController extends Controller
{

	public function __construct()
    {
        $this->refs = new Refs;
    }

    public function index()
    {
    	$ass = Pasien::where('status','register')->get();
        $ass2 = Assessment::all()->sortByDesc('tanggal');
    	return view('assessment.index')
            ->with('pasiens', $ass)
    		->with('assessments', $ass2);
    }

    public function form($ida=null)
    {
        $pasien = Pasien::where('status','register')->get();

        $anws = Assessment::where('id',$ida)->get()->first();

        $petugas = Petugas::where('jenis','=','therapist')->get();
    	$aq = AssessmentQuestion::all();
        $aqr = [];
        foreach($aq as $m){
            $aqr[$m->code] = $m;
        }
        
    	return view('assessment.form')
            ->with('aq', $aqr)
            ->with('pasiens', $pasien)
            ->with('petugas', $petugas)
    		->with('anw', $anws);
    }

    public function reg_assessment($idp=null)
    {
        $sel_pasien = null;
        $anws = null;
        if($idp)
        {
            $sel_pasien = Pasien::where('id',$idp)->get()->first();
            $tgl_as = Gen::to_date_time($sel_pasien->tanggal_assessment);
            $tgl_as = (!empty($tgl_as)) ? $tgl_as->format('Y-m-d'):date('Y-m-d');
            $anws = Assessment::where('pasien_id',$sel_pasien->id)
                ->where('petugas_id',$sel_pasien->petugas_assessment)
                ->where('tanggal',$tgl_as)->get()->first();

            if(count($anws)==0)
            {
                $anws = new Assessment;
                $anws->pasien_id = $sel_pasien->id;
                $anws->petugas_id = $sel_pasien->petugas_assessment;
                $anws->tanggal = $tgl_as;
                $anws->save();
            }

            return redirect()->route('formassessment',['ida'=>$anws->id]);
        }

        return redirect()->route('formassessment');
    }

    public function out($ida)
    {
        $mpdf = new \Mpdf\Mpdf();

        $anws = Assessment::find($ida);
        $jawab = $anws->answer;
        $aq = AssessmentQuestion::all();
        $aqr = [];
        foreach($aq as $m){
            $aqr[$m->code] = $m;
        }
        $anr = [];
        foreach($jawab as $m){
            $anr[$m->qcode] = $m;
        }
        
        $data = view('assessment.output')
            ->with('ass',$anws)
            ->with('aq',$aqr)
            ->with('an',$anr);
        $mpdf->SetTitle('Assessment ' . $anws->pasien->nama_lengkap);

        $mpdf->WriteHTML($data->render());
        $mpdf->Output();
        exit;
    }

    
}
