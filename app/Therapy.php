<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Therapy extends Model
{
    use Notifiable;

    protected $fillable = ['tanggal','assessment_id','pasien_id','petugas_id','created_at','updated_at','description'];

    protected $hidden = [];

    public function pasien()
    {
        return $this->belongsTo('App\Pasien', 'pasien_id');
    }    

    public function petugasTerapi()
    {
        return $this->belongsTo('App\Petugas', 'petugas_id');
    }

    public function assessment()
    {
        return $this->belongsTo('App\Assessment', 'assessment_id');
    }    

}
