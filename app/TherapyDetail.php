<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class TherapyDetail extends Model
{
    use Notifiable;

    protected $fillable = ['therapies_id','program','aktivitas','keterangan'];

    protected $hidden = [];

    public function terapi()
    {
        return $this->belongsTo('App\Therapy', 'therapies_id');
    }    

}
