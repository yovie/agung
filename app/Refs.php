<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Refs extends Model
{
    
	public function agama()
	{
		return [
			'islam' => 'Islam',
			'katolik' => 'Kristes Katolik',
			'protestan' => 'Kristes Protestan',
			'hindu' => 'Hindu',
			'budha' => 'Budha',
			'kepercayaan' => 'Kepercayaan'
		];
	}

	public function kewarganegaraan()
	{
		return [
			'wni' => 'WNI (Warga Negara Indonesia)',
			'wna' => 'WNA (Warga Negara Asing)'
		];
	}

	public function goldar()
	{
		return [
			'A' => 'A',
			'B' => 'B',
			'AB' => 'AB',
			'O' => 'O'
		];
	}

	public function pendidikan()
	{
		return [
			'tidak' => 'Tidak Sekolah',
			'sd' => 'SD/MI/Sederajat',
			'smp' => 'SMP/MTs/Sederajat',
			'sma' => 'SMA/SMK/MA/Sederajat',
			'd1' => 'D1',
			'd2' => 'D2',
			'd3' => 'D3',
			's1' => 'S1',
			's2' => 'S2',
			's3' => 'S3',
			'prof' => 'Professor'
		];
	}

}
