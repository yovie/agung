<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerNewPolicies();
    }
    
    
    public function registerNewPolicies()
    {
        Gate::define('user-management', function ($user) {
            return $user->group_id==env('ADMIN');
        });

        Gate::define('registrasi', function ($user) {
            return $user->group_id==env('REGISTER')
                || $user->group_id==env('ADMIN');
        });
        Gate::define('assessment', function ($user) {
            return $user->group_id==env('ASSESSMENT')
                || $user->group_id==env('ADMIN');
        });
        Gate::define('terapi', function ($user) {
            return $user->group_id==env('THERAPIST')
                || $user->group_id==env('ADMIN');
        });
        Gate::define('jadwal', function ($user) {
            return $user->group_id==env('THERAPIST')
                || $user->group_id==env('ADMIN')
                || $user->group_id==env('ASSESSMENT')
                || $user->group_id==env('REGISTER');
        });
    }
}
