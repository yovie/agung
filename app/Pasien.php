<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use App\Assessment;

class Pasien extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tanggal_assessment', 'petugas_assessment', 'jam_assessment', 'np', 'nama_lengkap', 'nama_panggilan', 'gender', 'tempat_lahir', 'tanggal_lahir', 'agama', 'kewarganegaraan', 'anak_ke', 'jumlah_saudara_kandung', 'jumlah_saudara_tiri', 'jumlah_saudara_angkat', 'bahasa_sehari', 'berat_badan', 'tinggi_badan', 'golongan_darah', 'penyakit_berat', 'berkebutuhan_khusus', 'alamat', 'telepon', 'rt', 'rw', 'nama_dusun', 'nama_kelurahan', 'kecamatan', 'kode_pos', 'model_transportasi', 'tinggal_pada', 'nama_ayah_kandung', 'nama_ibu_kandung', 'pendidikan_ayah', 'pendidikan_ibu', 'pekerjaan_ayah', 'pekerjaan_ibu', 'penghasilan_ayah', 'penghasilan_ibu', 'tempatlahir_ayah', 'tempatlahir_ibu', 'tanggallahir_ayah', 'tanggallahir_ibu', 'nama_wali', 'pendidikan_wali', 'hubungan_wali', 'pekerjaan_wali', 'penghasilan_wali', 'masuk_sebagai', 'asal_anak', 'nama_tk', 'nomor_tahun_surat', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];


    public function petugasAssessment()
    {
        return $this->belongsTo('App\Petugas', 'petugas_assessment');
    }

    public function getAssessment()
    {
        $assessment = Assessment::where('pasien_id',$this->id)->where('tanggal',$this->tanggal_assessment)->get()->first();
        return $assessment;
    }

}
