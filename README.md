# Sistem Antrian Terintegrasi (SAT)

## Instalation
- clone repository
- create database agung
- sesuaikan config/database.php
- ``` php artisan migrate:install --seed ```
- ``` php artisan serve ```

## CSS and JS Generator
- config webpack.mix.js
- ``` npm run production ```

## Query Notes
```sql
SELECT `COLUMN_NAME` 
	FROM `INFORMATION_SCHEMA`.`COLUMNS` 
WHERE 
	`TABLE_SCHEMA`='agung' AND `TABLE_NAME`='pasiens';
```

## Paths
- database/migrations/
- app/Helper
- config/app.php

## Laravel Commands
```bash
php artisan migrate:install
php artisan migrate:reset
php artisan migrate:fresh --seed
php artisan serve
```

## Snippets
```php
Auth::user()
Model::find()
Model::where()
Model::all()
```

## Bisnis proses
- siswa registrasi, dapat jadwal assessment
- siswa assessment, dapat jadwal terapi
- siswa terapi, hasil terapi
	- home program

## Progress
- menu evaluasi2

## Problem
- none

## Install on Apache
`sudo ln -s <laravel>/public /var/www/html/appname