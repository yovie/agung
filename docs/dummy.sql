-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 11, 2017 at 09:38 AM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

set foreign_key_checks=false;


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agung`
--

--
-- Dumping data for table `aq_answers`
--

INSERT INTO `aq_answers` (`id`, `aq`, `qcode`, `answer`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(22, 2, 'A1', 'uy', '2017-11-29 07:14:08', '2017-11-29 07:14:20', NULL, 1),
(23, 2, 'B', 'a', '2017-11-29 07:14:08', '2017-12-02 14:28:29', NULL, 1),
(24, 2, 'C1', 'a', '2017-11-29 07:14:08', '2017-12-02 14:28:29', NULL, 1),
(25, 2, 'C2', 'a', '2017-11-29 07:14:09', '2017-12-02 14:28:30', NULL, 1),
(26, 2, 'C3', 'a', '2017-11-29 07:14:09', '2017-12-02 14:28:30', NULL, 1),
(27, 2, 'C4', 'a', '2017-11-29 07:14:09', '2017-12-02 14:28:30', NULL, 1),
(28, 2, 'D1', 'a', '2017-11-29 07:14:09', '2017-12-02 14:28:31', NULL, 1),
(29, 2, 'D2', 'a', '2017-11-29 07:14:09', '2017-12-02 14:28:31', NULL, 1),
(30, 2, 'D3', 'a', '2017-11-29 07:14:09', '2017-12-02 14:28:31', NULL, 1),
(31, 2, 'D4', 'a', '2017-11-29 07:14:09', '2017-12-02 14:28:31', NULL, 1),
(32, 2, 'D5', 'a', '2017-11-29 07:14:09', '2017-12-02 14:28:32', NULL, 1),
(33, 2, 'E1', 'a', '2017-11-29 07:14:09', '2017-12-02 14:28:32', NULL, 1),
(34, 2, 'E2', 'a', '2017-11-29 07:14:09', '2017-12-02 14:28:32', NULL, 1),
(35, 2, 'E3', 'a', '2017-11-29 07:14:10', '2017-12-02 14:28:32', NULL, 1),
(36, 2, 'E4', 'a', '2017-11-29 07:14:10', '2017-12-02 14:28:33', NULL, 1),
(37, 2, 'E5', 'a', '2017-11-29 07:14:10', '2017-12-02 14:28:33', NULL, 1),
(38, 2, 'E6', 'a', '2017-11-29 07:14:10', '2017-12-02 14:28:33', NULL, 1),
(39, 2, 'F', 'a', '2017-11-29 07:14:10', '2017-12-02 14:28:33', NULL, 1),
(40, 2, 'G', 'a', '2017-11-29 07:14:10', '2017-12-02 14:28:34', NULL, 1),
(41, 2, 'H', 'a', '2017-11-29 07:14:10', '2017-12-02 14:28:34', NULL, 1),
(42, 2, 'I', 'a', '2017-11-29 07:14:10', '2017-12-02 14:28:34', NULL, 1),
(43, 3, 'A1', 'sip', '2017-11-29 07:21:47', '2017-11-29 07:30:56', NULL, 1),
(44, 3, 'B', 'yuhu', '2017-11-29 07:21:47', '2017-12-03 04:05:49', NULL, 1),
(45, 3, 'C1', NULL, '2017-11-29 07:21:48', '2017-11-29 07:21:48', NULL, NULL),
(46, 3, 'C2', NULL, '2017-11-29 07:21:48', '2017-11-29 07:21:48', NULL, NULL),
(47, 3, 'C3', NULL, '2017-11-29 07:21:48', '2017-11-29 07:21:48', NULL, NULL),
(48, 3, 'C4', NULL, '2017-11-29 07:21:48', '2017-11-29 07:21:48', NULL, NULL),
(49, 3, 'D1', NULL, '2017-11-29 07:21:48', '2017-11-29 07:21:48', NULL, NULL),
(50, 3, 'D2', NULL, '2017-11-29 07:21:48', '2017-11-29 07:21:48', NULL, NULL),
(51, 3, 'D3', NULL, '2017-11-29 07:21:49', '2017-11-29 07:21:49', NULL, NULL),
(52, 3, 'D4', NULL, '2017-11-29 07:21:49', '2017-11-29 07:21:49', NULL, NULL),
(53, 3, 'D5', NULL, '2017-11-29 07:21:49', '2017-11-29 07:21:49', NULL, NULL),
(54, 3, 'E1', NULL, '2017-11-29 07:21:49', '2017-11-29 07:21:49', NULL, NULL),
(55, 3, 'E2', NULL, '2017-11-29 07:21:49', '2017-11-29 07:21:49', NULL, NULL),
(56, 3, 'E3', NULL, '2017-11-29 07:21:49', '2017-11-29 07:21:49', NULL, NULL),
(57, 3, 'E4', NULL, '2017-11-29 07:21:49', '2017-11-29 07:21:49', NULL, NULL),
(58, 3, 'E5', NULL, '2017-11-29 07:21:49', '2017-11-29 07:21:49', NULL, NULL),
(59, 3, 'E6', NULL, '2017-11-29 07:21:49', '2017-11-29 07:21:49', NULL, NULL),
(60, 3, 'F', NULL, '2017-11-29 07:21:49', '2017-11-29 07:21:49', NULL, NULL),
(61, 3, 'G', NULL, '2017-11-29 07:21:50', '2017-11-29 07:21:50', NULL, NULL),
(62, 3, 'H', NULL, '2017-11-29 07:21:50', '2017-11-29 07:21:50', NULL, NULL),
(63, 3, 'I', NULL, '2017-11-29 07:21:50', '2017-11-29 07:21:50', NULL, NULL);

--
-- Dumping data for table `assessments`
--

INSERT INTO `assessments` (`id`, `tanggal`, `tanggal_terapi`, `petugas_terapi_id`, `pasien_id`, `petugas_id`, `description`, `created_at`, `updated_at`) VALUES
(2, '2017-12-09', '2017-12-14', 2, 1, 1, 'asesment lagi', '2017-11-29 07:14:08', '2017-12-02 14:51:43'),
(3, '2017-12-07', NULL, NULL, 2, 1, NULL, '2017-11-29 07:21:47', '2017-11-29 07:21:47');

--
-- Dumping data for table `assessment_questions`
--

INSERT INTO `assessment_questions` (`id`, `code`, `question`) VALUES
(1, 'A', 'Apakah masa kehamilan ibunda bermasalah? (Bermasalah/ Tidak bermasalah)'),
(2, 'A1', 'Bermasalah, ceritakan permasalahannya'),
(3, 'B', 'Apakah kehamilan ini kehamilan yang diinginkan?'),
(4, 'C', 'Riwayat kehamilan'),
(5, 'C1', 'Ibu hamil saat usia?'),
(6, 'C2', 'Apakah ibu mengalami sakit saat hamil?'),
(7, 'C3', 'Apakah ibu mengalami trauma saat masa kehamilan?'),
(8, 'C4', 'Apakah ibu mengkonsumsi obat-obatan atau jamu?'),
(9, 'D', 'Riwayat kelahiran'),
(10, 'D1', 'Bagaimana proses kelahiran anak? Normal/ Caesar/ Menggunakan alat bantu?'),
(11, 'D2', 'Apakah anak langsung menangis saat lahir?'),
(12, 'D3', 'Apakah anak kuning saat lahir?'),
(13, 'D4', 'Berat dan panjang anak saat lahir?'),
(14, 'D5', 'Apakah ada hal penting lain yang perlu diketahui tentang anak?'),
(15, 'E', 'Riwayat perkembangan bahasa'),
(16, 'E1', 'Mengoceh, usia :'),
(17, 'E2', 'Kata-kata pertama yang muncul, usia :'),
(18, 'E3', 'Kalimat dengan 2 kata, usia :'),
(19, 'E4', 'Pengucapan jelas atau tidak jelas, usia :'),
(20, 'E5', 'Mengucapkan kalimat sederhana (Mampu/ Belum mampu), usia :'),
(21, 'E6', 'Gagap atau tidak, usia :'),
(22, 'F', 'Adakah keluarga lain dalam keluarga inti yang tinggal dengan anak dalam satu rumah?'),
(23, 'G', 'Apakah anak bermain dengan orang tua?'),
(24, 'H', 'Bagaimana perilaku di rumah? Aktif, pasif, sulit diatur, mudah diatur, mau bermain dengan saudara atau tidak?'),
(25, 'I', 'Masalah perilaku lain di rumah, jelaskan (ketika marahdan ketika senang)');

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'ADMIN', 'Administrator', NULL, NULL),
(2, 'REGISTER', 'Registrasi siswa', NULL, NULL),
(3, 'ASSESSMENT', 'Assessment siswa', NULL, NULL),
(4, 'THERAPIST', 'Terapis', NULL, NULL);

--
-- Dumping data for table `pasiens`
--

INSERT INTO `pasiens` (`id`, `tanggal_assessment`, `petugas_assessment`, `jam_assessment`, `np`, `nama_lengkap`, `nama_panggilan`, `gender`, `tempat_lahir`, `tanggal_lahir`, `agama`, `kewarganegaraan`, `anak_ke`, `jumlah_saudara_kandung`, `jumlah_saudara_tiri`, `jumlah_saudara_angkat`, `bahasa_sehari`, `berat_badan`, `tinggi_badan`, `golongan_darah`, `penyakit_berat`, `berkebutuhan_khusus`, `alamat`, `telepon`, `rt`, `rw`, `nama_dusun`, `nama_kelurahan`, `kecamatan`, `kode_pos`, `model_transportasi`, `tinggal_pada`, `nama_ayah_kandung`, `nama_ibu_kandung`, `pendidikan_ayah`, `pendidikan_ibu`, `pekerjaan_ayah`, `pekerjaan_ibu`, `penghasilan_ayah`, `penghasilan_ibu`, `tempatlahir_ayah`, `tempatlahir_ibu`, `tanggallahir_ayah`, `tanggallahir_ibu`, `nama_wali`, `pendidikan_wali`, `hubungan_wali`, `pekerjaan_wali`, `penghasilan_wali`, `masuk_sebagai`, `asal_anak`, `nama_tk`, `nomor_tahun_surat`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, '2017-12-09', 1, '07:00:00', NULL, 'Asep asep', 'asep', 'L', 'Bandung', '2000-11-26', 'islam', 'wni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A', NULL, NULL, 'Bandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Dodi', 'Sari', 'tidak', 'tidak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tidak', NULL, NULL, NULL, 'Siswa', NULL, NULL, NULL, 'assessment', '2017-11-26 04:04:29', '2017-12-03 04:02:06', 1, NULL),
(2, '2017-12-07', 1, '09:00:00', NULL, 'Jaka Pratama', 'Jaka', 'L', 'Bandung', '2001-11-01', 'islam', 'wni', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'A', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Pratama', 'Pratiwi', 'tidak', 'tidak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'tidak', NULL, NULL, NULL, 'Siswa', NULL, NULL, NULL, 'register', '2017-11-29 07:21:00', '2017-11-29 07:21:15', 1, NULL);

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`id`, `nip`, `nama`, `hp`, `email`, `jenis`, `created_at`, `updated_at`) VALUES
(1, '111', 'Joko', NULL, NULL, 'assessment', '2017-11-26 04:03:10', '2017-11-26 04:03:10'),
(2, '222', 'Bodo', NULL, NULL, 'therapist', '2017-11-26 04:03:17', '2017-11-26 04:03:17');

--
-- Dumping data for table `therapies`
--

INSERT INTO `therapies` (`id`, `tanggal`, `assessment_id`, `pasien_id`, `petugas_id`, `created_at`, `updated_at`, `jenis`, `description`) VALUES
(1, '2017-12-14', 2, 1, 2, '2017-12-10 09:25:58', '2017-12-10 23:39:38', 'Terapi Wicara Aja', 'Keterangan terapi wicara saja');

--
-- Dumping data for table `therapy_details`
--

INSERT INTO `therapy_details` (`id`, `therapies_id`, `program`, `aktivitas`, `keterangan`, `updated_at`, `created_at`) VALUES
(1, 1, 'ada apa', 'apa anda', 'dengan saya', '2017-12-10 14:30:36', '2017-12-10 14:27:42'),
(4, 1, 'sd', 'ds', 'ds', '2017-12-10 14:35:24', '2017-12-10 14:35:24'),
(5, 1, 'asa', 'aja', 'ada', '2017-12-10 23:21:16', '2017-12-10 23:21:16');

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `active`, `group_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$IZRo3mUmUEYB9sgQnGDVQOwNd6FEt1H7EXmWZB14VoL7cBkdsj1nC', 1, 1, 'EMCXo4alhJPPdTOX4x8a4r2TssFcB1tU996kCiAqMNHkkNAB0gJsjp6dO4Td', NULL, NULL),
(2, 'register', 'register@gmail.com', '$2y$10$Ki5ilDPMoKwhWC1uU/xxDO3ylPfQybzr4GrpJ57hcNvTgoFHkVnKu', 1, 2, NULL, NULL, NULL),
(3, 'assessment', 'assessment@gmail.com', '$2y$10$5Xa1mRDdYK2UisK6bXtPSeky/0RY.PSHDhxy818nHmLosohtRhiYO', 1, 3, NULL, NULL, NULL),
(4, 'therapist', 'therapist@gmail.com', '$2y$10$vzyRhQ4uNkZ/B6ZPyLwzouGdl5gvCczlGh4k2N.E8.Sy75U6oWGWm', 1, 4, NULL, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
